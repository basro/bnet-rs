// #![feature(generic_associated_types)]
// #![feature(type_alias_impl_trait)]

use std::time::Duration;

use layer0::{
    endpoint::Endpoint,
    runtime::tokio::TokioRuntime,
    socket::{SimulatedConditions, SimulatedNetworkConditionsSocket},
};
use tokio::net::UdpSocket;

type SimulatedSocket = SimulatedNetworkConditionsSocket<UdpSocket, TokioRuntime>;
type TokioEndpoint<T> = Endpoint<T, TokioRuntime>;

#[tokio::main]
async fn main() {
    tokio::spawn(async {
        tokio::time::sleep(Duration::from_millis(250)).await;
        let simulated_sock = SimulatedSocket::new(
            UdpSocket::bind("0.0.0.0:0").await.unwrap(),
            SimulatedConditions {
                delay: Duration::from_secs(1),
                packet_loss: 0.1,
            },
        );
        let (endpoint, _) = TokioEndpoint::new(simulated_sock);
        let conn = endpoint
            .connect("127.0.0.1:9999".parse().unwrap())
            .await
            .unwrap();

        let sender = async {
            let mut counter = 0;
            loop {
                counter += 1;
                if let Ok(pn) = conn.send(5, format!("hello {}", counter).as_bytes()).await {
                    println!("[Client App] sent pn: {}", pn);
                }
                tokio::time::sleep(Duration::from_secs(2)).await;
            }
        };

        let receiver = {
            async {
                while let Some(packet) = conn.recv().await {
                    println!(
                        "[Client App] received pn: {}, code: {}, data: {}",
                        packet.packet_number,
                        packet.code,
                        std::str::from_utf8(packet.data()).unwrap_or_default()
                    );
                }
            }
        };

        tokio::select! {
            _ = sender => {}
            _ = receiver => {}
            _ = tokio::time::sleep(Duration::from_secs(5)) => {}
        };

        println!("[Client App] ended");
    });

    let host = async {
        let (_, mut h) =
            TokioEndpoint::<UdpSocket>::new(UdpSocket::bind("0.0.0.0:9999").await.unwrap());
        while let Some(conn_request) = h.recv().await {
            let conn = conn_request.accept();

            tokio::spawn(async move {
                loop {
                    if let Some(packet) = conn.recv().await {
                        println!(
                            "[Host App] received pn: {} code: {}",
                            packet.packet_number, packet.code
                        );
                        if let Ok(pn) = conn.try_send(11, packet.data()) {
                            println!("[Host App] sent pn: {}", pn);
                        }
                    }
                }
            });
            println!("[Host App] new connection accepted");
        }
        println!("[Host App] end");
    };

    host.await;
}
