#![feature(let_chains)]
#![feature(type_alias_impl_trait)]

mod buffers;
pub mod connection;
mod crypto;
pub mod endpoint;
mod packet;
mod packet_number;
mod received_packets_set;
pub mod runtime;
pub mod simulated_conditions_channel;
pub mod socket;
#[cfg(test)]
mod test_utils;

use std::time::Duration;

/// How long a challenge token is valid for
const CONNECTION_CHALLENGE_DURATION: Duration = Duration::from_secs(10);

/// The maximum length of UDP messages bnet layer0 will send or accept
///
/// This length includes bnet layer0 headers but does not include UDP and IP headers
pub const MAX_UDP_MESSAGE_LENGTH: usize = 1300;

/// The max length (in bytes) that a payload packet may need to encode its header
pub const MAX_PAYLOAD_HEADER_SIZE: usize = 1 + 8 + 16; // byte header + packet number length + encryption tag

/// The maximum length allowed for the payload in a payload packet
pub const MAX_PAYLOAD_LENGTH: usize = MAX_UDP_MESSAGE_LENGTH - MAX_PAYLOAD_HEADER_SIZE;
