use std::{future::Future, time::Duration};

use runtime_trait::tokio::tracked::TrackedTokioRuntime;
use tokio::time::Instant;

pub fn drive_async_test(future: impl Future) {
    TrackedTokioRuntime::block_on_and_wait_for_tasks(
        async {
            tokio::time::pause();
            future.await;
        },
        Duration::from_secs(30),
    )
    .expect("No tasks should leak")
}

#[allow(unused)]
pub fn instant_is_approx(a: Instant, b: Instant) -> bool {
    let max = Duration::from_millis(50);
    a - b < max && b - a < max
}
