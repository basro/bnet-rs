use aes_gcm::Tag;
use codec::{Decode, Encode, Reader, Remaining, Writer};
use once_cell::sync::Lazy;
use siphasher::sip::SipHasher;
use std::{
    net::SocketAddr,
    ops::RangeInclusive,
    time::{Duration, Instant},
};

use crate::{crypto::ConnectionCipher, CONNECTION_CHALLENGE_DURATION, MAX_PAYLOAD_HEADER_SIZE};

#[derive(PartialEq, Eq, Debug)]
pub struct ConnRequestData {
    pub version: u32,
    pub request_id: u64,
}

const CONNREQ_MAGIC_NUMBER: u32 = 0xB1E10001;
const CONNREQ_MESSAGE_SIZE: usize = 1199; // 1200 - 1 byte taken in the header_byte
const CONNREQ_PADDING_SIZE: usize = CONNREQ_MESSAGE_SIZE - 16;

impl Decode for ConnRequestData {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        if reader.remaining_bytes() != CONNREQ_MESSAGE_SIZE {
            return Err(codec::Error::InvalidData);
        }
        let magic_number: u32 = reader.read_le()?;

        if magic_number != CONNREQ_MAGIC_NUMBER {
            return Err(codec::Error::InvalidData);
        }

        let version = reader.read_le()?;
        let request_id = reader.read_le()?;
        reader.skip(CONNREQ_PADDING_SIZE)?;

        Ok(Self {
            version,
            request_id,
        })
    }
}

impl Encode for ConnRequestData {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.write_le(CONNREQ_MAGIC_NUMBER)?;
        writer.write_le(self.version)?;
        writer.write_le(self.request_id)?;
        writer.skip(CONNREQ_PADDING_SIZE)
    }
}

static CHALLENGE_EPOCH: Lazy<Instant> = Lazy::new(Instant::now);

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
pub struct Challenge {
    pub connection_id: u64,
    pub timestamp: u64,
    pub hash: u64,
}

impl Challenge {
    pub fn new(connection_id: u64, addr: &SocketAddr, key: &[u8; 16]) -> Self {
        let timestamp = CHALLENGE_EPOCH.elapsed().as_secs();
        let hash = Self::compute_challenge_hash(connection_id, addr, timestamp, key);
        Self {
            connection_id,
            timestamp,
            hash,
        }
    }

    pub fn validate(&self, addr: &SocketAddr, key: &[u8; 16]) -> bool {
        let hash = Self::compute_challenge_hash(self.connection_id, addr, self.timestamp, key);
        let time_elapsed_since_timestamp = CHALLENGE_EPOCH
            .elapsed()
            .saturating_sub(Duration::from_secs(self.timestamp));
        hash == self.hash && time_elapsed_since_timestamp < CONNECTION_CHALLENGE_DURATION
    }

    fn compute_challenge_hash(
        connection_id: u64,
        addr: &SocketAddr,
        timestamp: u64,
        key: &[u8; 16],
    ) -> u64 {
        let mut hasher = SipHasher::new_with_key(key);

        use core::hash::Hasher;
        match addr.ip() {
            std::net::IpAddr::V4(ip) => hasher.write(&ip.octets()),
            std::net::IpAddr::V6(ip) => hasher.write(&ip.octets()),
        };
        hasher.write_u16(addr.port());
        hasher.write_u64(connection_id);
        hasher.write_u64(timestamp);
        hasher.finish()
    }
}

impl Decode for Challenge {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        Ok(Self {
            connection_id: reader.read_le()?,
            timestamp: reader.read_le()?,
            hash: reader.read_le()?,
        })
    }
}

impl Encode for Challenge {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.write_le(self.connection_id)?;
        writer.write_le(self.timestamp)?;
        writer.write_le(self.hash)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct ConnChallengeData {
    pub challenge: Challenge,
}

impl Decode for ConnChallengeData {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        let challenge = reader.decode()?;
        Ok(Self { challenge })
    }
}

impl Encode for ConnChallengeData {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        self.challenge.encode(writer)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct ConnRequestFullData {
    pub challenge: Challenge,
    pub request_id: u64,
    pub pub_key: [u8; 32],
    // TODO: Add user data (encrypted?)
}

impl Decode for ConnRequestFullData {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        Ok(Self {
            challenge: reader.decode()?,
            request_id: reader.read_le()?,
            pub_key: reader.read_array()?,
        })
    }
}

impl Encode for ConnRequestFullData {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.encode(&self.challenge)?;
        writer.write_le(self.request_id)?;
        writer.write(&self.pub_key)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct ConnRejectData {
    pub request_id: u64,
    pub code: u32,
}

impl Decode for ConnRejectData {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        Ok(Self {
            request_id: reader.read_le()?,
            code: reader.read_le()?,
        })
    }
}

impl Encode for ConnRejectData {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.write_le(self.request_id)?;
        writer.write_le(self.code)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct ConnAcceptData {
    pub request_id: u64,
    pub pub_key: [u8; 32],
}

impl Decode for ConnAcceptData {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        Ok(Self {
            request_id: reader.read_le()?,
            pub_key: reader.read_array()?,
        })
    }
}

impl Encode for ConnAcceptData {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.write_le(self.request_id)?;
        writer.write(&self.pub_key)
    }
}

#[derive(PartialEq, Eq, Debug)]
pub struct PayloadData<'storage> {
    truncated_packet_number: u64,
    pub packet_number_len: usize,
    pub code: u8,
    pub payload: &'storage mut [u8],
}

impl<'storage> PayloadData<'storage> {
    fn decode(data: &'storage mut [u8], packet_number_len: usize, code: u8) -> codec::Result<Self> {
        let mut reader: &[u8] = data;
        let mut packet_number_bytes = [0; 8];
        reader.read(&mut packet_number_bytes[0..packet_number_len])?;

        let truncated_packet_number = u64::from_le_bytes(packet_number_bytes);

        let payload = &mut data[packet_number_len..];

        Ok(Self {
            truncated_packet_number,
            packet_number_len,
            payload,
            code,
        })
    }

    pub fn decrypt(
        self,
        cipher: &ConnectionCipher,
        packet_number: u64,
    ) -> Result<&'storage mut [u8], ()> {
        let (tag, payload) = self.payload.split_at_mut(16);

        cipher
            .decrypt_in_place_detached(packet_number, payload, Tag::from_slice(tag))
            .map_err(|_| ())?;
        Ok(payload)
    }

    pub fn expand_packet_number(&self, expected_packet_number: u64) -> u64 {
        expand_truncated_packet_number(
            expected_packet_number,
            self.truncated_packet_number,
            self.packet_number_len,
        )
    }
}

#[derive(PartialEq, Debug, Clone, Copy)]
enum PacketType {
    ConnRequest,
    ConnChallenge,
    ConnRequestFull,
    ConnReject,
    ConnAccept,
    Payload { code: u8 },
}

const PACKET_TYPE_MAX_VALUE: u8 = 31;
const PACKET_TYPE_PAYLOAD_RANGE: RangeInclusive<u8> = 8..=PACKET_TYPE_MAX_VALUE;
const PAYLOAD_CODE_MAX_VALUE: u8 = PACKET_TYPE_MAX_VALUE - 8;

impl PacketType {
    pub fn make_valid_payload_or_panic(code: u8) -> Self {
        assert!(code <= PAYLOAD_CODE_MAX_VALUE);
        Self::Payload { code }
    }
}

impl TryFrom<u8> for PacketType {
    type Error = ();

    fn try_from(v: u8) -> Result<Self, Self::Error> {
        use PacketType::*;
        match v {
            0 => Ok(ConnRequest),
            1 => Ok(ConnChallenge),
            2 => Ok(ConnRequestFull),
            3 => Ok(ConnReject),
            4 => Ok(ConnAccept),
            // 5 to 7 are reserved
            v if PACKET_TYPE_PAYLOAD_RANGE.contains(&v) => Ok(Payload {
                code: v - PACKET_TYPE_PAYLOAD_RANGE.start(),
            }),
            _ => Err(()),
        }
    }
}

impl From<PacketType> for u8 {
    fn from(pt: PacketType) -> Self {
        use PacketType::*;
        match pt {
            ConnRequest => 0,
            ConnChallenge => 1,
            ConnRequestFull => 2,
            ConnReject => 3,
            ConnAccept => 4,
            Payload { code } => code + 8,
        }
    }
}

#[derive(PartialEq, Eq, Debug)]
pub enum Packet<'storage> {
    ConnRequest(ConnRequestData),
    ConnChallenge(ConnChallengeData),
    ConnRequestFull(ConnRequestFullData),
    ConnReject(ConnRejectData),
    ConnAccept(ConnAcceptData),
    Payload(PayloadData<'storage>),
}

impl From<&Packet<'_>> for PacketType {
    fn from(p: &Packet) -> Self {
        use Packet::*;
        match p {
            ConnRequest(_) => Self::ConnRequest,
            ConnChallenge(_) => Self::ConnChallenge,
            ConnRequestFull(_) => Self::ConnRequestFull,
            ConnReject(_) => Self::ConnReject,
            ConnAccept(_) => Self::ConnAccept,
            Payload(data) => Self::Payload { code: data.code },
        }
    }
}

impl<'storage> Packet<'storage> {
    pub fn decode(data: &'storage mut [u8]) -> codec::Result<Self> {
        if data.len() < 2 {
            return Err(codec::Error::UnexpectedEnd);
        }

        let (packet_type, packet_number_len) =
            decode_header_byte(data[0]).map_err(|_| codec::Error::InvalidData)?;

        let data = &mut data[1..];
        use Packet::*;

        if let PacketType::Payload { code } = packet_type {
            return Ok(Payload(PayloadData::decode(data, packet_number_len, code)?));
        }

        let mut reader: &[u8] = data;
        let decoded = match packet_type {
            PacketType::ConnRequest => ConnRequest(reader.decode()?),
            PacketType::ConnChallenge => ConnChallenge(reader.decode()?),
            PacketType::ConnRequestFull => ConnRequestFull(reader.decode()?),
            PacketType::ConnReject => ConnReject(reader.decode()?),
            PacketType::ConnAccept => ConnAccept(reader.decode()?),
            PacketType::Payload { .. } => unreachable!(),
        };

        if reader.remaining_bytes() != 0 {
            return Err(codec::Error::InvalidData);
        }
        Ok(decoded)
    }

    pub fn encode(&self, buf: &mut [u8]) -> codec::Result<usize> {
        let packet_type: PacketType = self.into();
        buf[0] = encode_header_byte(packet_type, 1);

        let mut writer = &mut buf[1..];

        use Packet::*;
        match self {
            ConnRequest(data) => data.encode(&mut writer)?,
            ConnChallenge(data) => data.encode(&mut writer)?,
            ConnRequestFull(data) => data.encode(&mut writer)?,
            ConnReject(data) => data.encode(&mut writer)?,
            ConnAccept(data) => data.encode(&mut writer)?,
            Payload(_) => panic!(),
        };

        let remaining_bytes = writer.remaining_bytes();

        Ok(buf.len() - remaining_bytes)
    }
}

/// Encodes a payload packet into the data slice.
///
/// payload_start_offset marks the position in the data slice where the payload bytes start.
/// The payload bytes extend up to the end of the data slice and will be encrypted in place.
///
/// The payload packet header is then prepended right before the payload_start_offset.
///
/// Returns the position in the data slice where the payload packet header starts.
pub fn encode_payload_packet_in_place(
    code: u8,
    packet_number: u64,
    largest_acked: u64,
    cipher: &ConnectionCipher,
    data: &mut [u8],
    payload_start_offset: usize,
) -> usize {
    assert!(
        payload_start_offset >= MAX_PAYLOAD_HEADER_SIZE,
        "Payload offset should leave enough space for the header"
    );
    let packet_number_len = compute_packet_number_len(packet_number, largest_acked);
    let header_byte: u8 = encode_header_byte(
        PacketType::make_valid_payload_or_panic(code),
        packet_number_len,
    );

    let aes_tag = cipher
        .encrypt_in_place_detached(packet_number, &mut data[payload_start_offset..])
        .unwrap();

    let start_pos = payload_start_offset - (packet_number_len + 1 + 16);
    let mut writer = &mut data[start_pos..];
    writer
        .write_le(header_byte)
        .expect("There's enough space for the header");
    writer
        .write(&packet_number.to_le_bytes()[..packet_number_len])
        .expect("There's enough space for the header");
    writer
        .write(&aes_tag)
        .expect("There's enough space for the header");

    start_pos
}

fn compute_packet_number_len(packet_number: u64, largest_acked: u64) -> usize {
    debug_assert!(packet_number > largest_acked);

    let diff = packet_number - largest_acked;

    if diff >= 1 << 31 {
        // If the difference is too large we do not truncate.
        // Return the exact number of bytes needed to encode the full packet number.
        // but make sure it's at least 5 bytes so that the other side knows we are sending without truncation.
        return std::cmp::max((8 - packet_number.leading_zeros() / 8) as usize, 5);
    }

    let range = diff * 2;
    debug_assert!(range < 1 << 32);
    std::cmp::max((8 - range.leading_zeros() / 8) as usize, 1)
}

fn expand_truncated_packet_number(expected: u64, truncated_pn: u64, pn_len: usize) -> u64 {
    // We have received a truncated packet number which is the the first `pn_len` bytes of a full
    // packet number. (Encoded in little endian, meaning they are the least significant bytes)
    //
    // We now have to guess what the most significant bytes are. If we guess wrong the packet
    // number will fail to decrypt and the packet will be lost.

    if pn_len > 4 {
        // They have sent the whole packet number so no guessing is needed.
        return truncated_pn;
    }

    // The expected packet number is the largest packet number we have received so far + 1.

    // Our guess will be to find the closest number to the expected one for which the least
    // significant bits match the received truncated packet.

    let win = 1 << (pn_len * 8);
    let hwin = win / 2;

    // candidate is a packet number formed by the combination of the high bits of expected and
    // low bits of truncated_pn.
    let mask = win - 1;
    let candidate = (expected & !mask) | truncated_pn;

    // candidate satisfies that the least significant bits match the truncated packet number but
    // candidate + win or candidate - win might be closer to the expected number so we check which
    // one is closer and return that.
    if let Some(diff) = expected.checked_sub(hwin) && candidate <= diff {
        candidate + win
    } else if candidate > expected + hwin && candidate > win {
        candidate - win
    } else {
        candidate
    }
}

fn decode_header_byte(header_byte: u8) -> Result<(PacketType, usize), ()> {
    let packet_number_len = (header_byte & 7) + 1;
    let packet_type_number = header_byte >> 3;

    Ok((packet_type_number.try_into()?, packet_number_len as usize))
}

fn encode_header_byte(packet_type: PacketType, packet_number_len: usize) -> u8 {
    assert!(
        packet_number_len != 0,
        "Packet number length should not be 0"
    );
    assert!(
        packet_number_len <= 8,
        "Packet number length should not be longer than 8 bytes"
    );

    let packet_type: u8 = packet_type.into();
    packet_type << 3 | ((packet_number_len - 1) as u8)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::MAX_UDP_MESSAGE_LENGTH;

    #[test]
    fn test_conn_request_packet_encoding() {
        let packet = Packet::ConnRequest(ConnRequestData {
            version: 1,
            request_id: 55,
        });

        let mut buf = [0; 1300];
        let size = packet.encode(&mut buf).unwrap();
        assert_eq!(Packet::decode(&mut buf[0..size]).unwrap(), packet);
        assert!(Packet::decode(&mut buf[0..1000]).is_err());
    }

    #[test]
    fn test_conn_challenge_packet_encoding() {
        let packet = Packet::ConnChallenge(ConnChallengeData {
            challenge: Challenge {
                connection_id: 5,
                timestamp: 0x1000200030004000,
                hash: 0xFF00FF00FF00FF00,
            },
        });

        let mut buf = [0; 1300];
        let size = packet.encode(&mut buf).unwrap();
        assert_eq!(Packet::decode(&mut buf[0..size]).unwrap(), packet);
        assert!(Packet::decode(&mut buf[0..1000]).is_err());
    }

    #[test]
    fn test_conn_request_full_encoding() {
        let mut pub_key = [0; 32];
        for (i, v) in pub_key.iter_mut().enumerate() {
            *v = i as u8;
        }

        let packet = Packet::ConnRequestFull(ConnRequestFullData {
            challenge: Challenge {
                connection_id: 5,
                timestamp: 0x1000200030004000,
                hash: 0xFF00FF00FF00FF00,
            },
            request_id: 55,
            pub_key,
        });

        let mut buf = [0; 1300];
        let size = packet.encode(&mut buf).unwrap();
        assert_eq!(Packet::decode(&mut buf[0..size]).unwrap(), packet);
        assert!(Packet::decode(&mut buf[0..1000]).is_err());
    }

    #[test]
    fn test_conn_reject_encoding() {
        let packet = Packet::ConnReject(ConnRejectData {
            request_id: 1203,
            code: 5,
        });

        let mut buf = [0; 1300];
        let size = packet.encode(&mut buf).unwrap();
        assert_eq!(Packet::decode(&mut buf[0..size]).unwrap(), packet);
        assert!(Packet::decode(&mut buf[0..1000]).is_err());
    }

    #[test]
    fn test_payload_encoding() {
        let mut buff = [0; MAX_UDP_MESSAGE_LENGTH];
        let mut writer = &mut buff[MAX_PAYLOAD_HEADER_SIZE..];
        let initial_len = writer.len();
        writer.write(b"hello world").unwrap();
        let written_bytes = initial_len - writer.len();
        let written_slice = &mut buff[..MAX_PAYLOAD_HEADER_SIZE + written_bytes];

        let cipher = ConnectionCipher::random_cipher();

        let pn = 0x10000;
        let largest_packet_received = 1200;
        let encoded_start_pos = encode_payload_packet_in_place(
            15,
            pn,
            1200,
            &cipher,
            written_slice,
            MAX_PAYLOAD_HEADER_SIZE,
        );

        match Packet::decode(&mut written_slice[encoded_start_pos..]).unwrap() {
            Packet::Payload(data) => {
                let packet_number = data.expand_packet_number(largest_packet_received);
                assert_eq!(packet_number, pn);

                assert_eq!(
                    data.decrypt(&cipher.flip(), packet_number).unwrap(),
                    b"hello world"
                );
            }
            _ => panic!("data should decode as payload"),
        }
    }

    #[test]
    fn test_header_byte() {
        let types = {
            use PacketType::*;
            [
                ConnRequest,
                ConnChallenge,
                ConnRequestFull,
                ConnReject,
                ConnAccept,
                Payload { code: 0 },
                Payload { code: 14 },
            ]
        };

        for t in types.into_iter() {
            for i in 1..9 {
                let encoded = encode_header_byte(t, i);
                let (decoded_pt, decoded_pn) = decode_header_byte(encoded).unwrap();
                assert_eq!(decoded_pt, t, "Packet type should be the same");
                assert_eq!(decoded_pn, i, "Packet number should be the same");
            }
        }

        assert!(
            decode_header_byte(0x5 << 3).is_err(),
            "packet type number 5 is reserved"
        );
        assert!(
            decode_header_byte(0x6 << 3).is_err(),
            "packet type number 6 is reserved"
        );
    }

    #[test]
    fn test_compute_packet_number_len() {
        fn is_boundary(pn: u64, la: u64, start_len: usize) -> bool {
            let a = compute_packet_number_len(pn, la);
            let b = compute_packet_number_len(pn + 1, la);
            a == start_len && b == start_len + 1
        }

        assert!(is_boundary(0x7F, 0, 1));
        assert!(is_boundary(0x7FFF, 0, 2));
        assert!(is_boundary(0x7FFFFF, 0, 3));
        assert!(is_boundary(0x7FFFFFFF, 0, 4));
        assert!(is_boundary(0xFFFFFFFFFF, 0, 5));
        assert!(is_boundary(0xFFFFFFFFFFFF, 0, 6));
        assert!(is_boundary(0xFFFFFFFFFFFFFF, 0, 7));

        assert!(is_boundary(0x7FFF, 0x7FFF - 0x7F, 1));
        assert!(is_boundary(0x7FFFFF, 0x7FFFFF - 0x7F, 1));
        assert!(is_boundary(0x7FFFFFFF, 0x7FFFFFFF - 0x7F, 1));
    }
}
