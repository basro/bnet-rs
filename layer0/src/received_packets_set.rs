#[derive(Debug, PartialEq, Eq)]
pub enum ReceivedPacketState {
    Forgotten,
    Received,
    Pending,
}

#[derive(Debug)]
pub struct ReceivedPacketsSet {
    end: u64,
    bits: u128, // Least significant bit represents packet <end - 1>, next is <end - 2>, all the way up to <end - 128>
}

impl ReceivedPacketsSet {
    pub fn new() -> Self {
        Self { end: 0, bits: 0 }
    }

    fn start(&self) -> u64 {
        if self.end < 128 {
            0
        } else {
            self.end - 128
        }
    }

    pub fn set(&mut self, packet_number: u64) {
        if packet_number < self.start() {
            return;
        }

        if packet_number >= self.end {
            let shift = packet_number - self.end + 1;
            if shift > 127 {
                self.bits = 0
            } else {
                self.bits <<= shift;
            }
            self.end += shift;
        }

        self.bits |= 1 << (self.end - packet_number - 1);
    }

    pub fn get(&self, packet_number: u64) -> ReceivedPacketState {
        if packet_number < self.start() {
            ReceivedPacketState::Forgotten
        } else if (packet_number >= self.end)
            || (self.bits & (1 << (self.end - packet_number - 1)) == 0)
        {
            ReceivedPacketState::Pending
        } else {
            ReceivedPacketState::Received
        }
    }

    pub fn get_next_expected_pn(&self) -> u64 {
        self.end
    }

    /*pub fn get_ack_set(&self) -> AckSet {
        AckSet {
            end: self.end,
            bits: self.bits as u64,
        }
    }*/
}

#[test]
fn test_received_packet_set() {
    let mut rps = ReceivedPacketsSet::new();
    assert_eq!(rps.get(0), ReceivedPacketState::Pending);
    rps.set(0);
    assert_eq!(rps.get(0), ReceivedPacketState::Received);
    assert_eq!(rps.get(10), ReceivedPacketState::Pending);
    rps.set(10);
    rps.set(127);
    assert_eq!(rps.get(0), ReceivedPacketState::Received);
    assert_eq!(rps.get(10), ReceivedPacketState::Received);
    assert_eq!(rps.get(127), ReceivedPacketState::Received);
    assert_eq!(rps.get(128), ReceivedPacketState::Pending);
    rps.set(128);
    rps.set(0); // Is ignored
    assert_eq!(rps.get(0), ReceivedPacketState::Forgotten);
    assert_eq!(rps.get(1), ReceivedPacketState::Pending);
    assert_eq!(rps.get(10), ReceivedPacketState::Received);
    assert_eq!(rps.get(126), ReceivedPacketState::Pending);
    assert_eq!(rps.get(127), ReceivedPacketState::Received);
    assert_eq!(rps.get(128), ReceivedPacketState::Received);
    assert_eq!(rps.get(129), ReceivedPacketState::Pending);
    rps.set(137);
    assert_eq!(rps.get(0), ReceivedPacketState::Forgotten);
    assert_eq!(rps.get(10), ReceivedPacketState::Received);
    assert_eq!(rps.get(127), ReceivedPacketState::Received);
    assert_eq!(rps.get(128), ReceivedPacketState::Received);
    assert_eq!(rps.get(137), ReceivedPacketState::Received);
    rps.set(5000);
    assert_eq!(rps.get(0), ReceivedPacketState::Forgotten);
    assert_eq!(rps.get(10), ReceivedPacketState::Forgotten);
    assert_eq!(rps.get(127), ReceivedPacketState::Forgotten);
    assert_eq!(rps.get(128), ReceivedPacketState::Forgotten);
    assert_eq!(rps.get(137), ReceivedPacketState::Forgotten);
    assert_eq!(rps.get(5000), ReceivedPacketState::Received);
}

/*
#[test]
fn test_received_packet_set_ack_set() {
    let mut rps = ReceivedPacketsSet::new();
    rps.set(5);
    rps.set(200);
    rps.set(205);
    rps.set(203);
    let ackset = rps.get_ack_set();
    assert_eq!(ackset.bits, 0b100101);
    assert_eq!(ackset.end, 206);
}

#[test]
fn test_ack_set_iter() {
    let mut rps = ReceivedPacketsSet::new();
    rps.set(4);
    rps.set(15);
    rps.set(22);

    let ackset = rps.get_ack_set();

    let mut iter = ackset.iter();
    assert_eq!(iter.next(), Some(22));
    assert_eq!(iter.next(), Some(15));
    assert_eq!(iter.next(), Some(4));
    assert_eq!(iter.next(), None);
}
*/
