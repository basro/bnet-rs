use std::{io, net::SocketAddr, sync::Arc, time::Duration};

use futures::{Future, FutureExt};
use tokio::{select, sync::oneshot};

use crate::{
    runtime::{Runtime, Socket},
    simulated_conditions_channel as scc,
    simulated_conditions_channel::SimulatedConditionsChannel,
};

pub use crate::simulated_conditions_channel::SimulatedConditions;

pub struct SimulatedNetworkConditionsSocket<SocketT: Socket, RuntimeT: Runtime> {
    outgoing: scc::Sender<(Box<[u8]>, SocketAddr), RuntimeT>,
    incoming: scc::Receiver<(Box<[u8]>, SocketAddr), RuntimeT>,
    _shutdown: oneshot::Sender<()>,
    sock: Arc<SocketT>,
}

impl<SocketT: Socket, RuntimeT: Runtime> SimulatedNetworkConditionsSocket<SocketT, RuntimeT> {
    pub fn new(sock: SocketT, config: SimulatedConditions) -> Self {
        let halved_config = config.halve();
        let (outgoing_s, outgoing_r) = SimulatedConditionsChannel::new(halved_config);
        let (incoming_s, incoming_r) = SimulatedConditionsChannel::new(halved_config);
        let (shutdown_s, shutdown_r) = oneshot::channel();
        let sock = Arc::new(sock);
        RuntimeT::spawn(simulated_network_conditions_socket_driver::<
            SocketT,
            RuntimeT,
        >(sock.clone(), shutdown_r, outgoing_r, incoming_s));
        Self {
            outgoing: outgoing_s,
            incoming: incoming_r,
            _shutdown: shutdown_s,
            sock,
        }
    }

    pub fn set_packet_loss(&self, p: f32) {
        let halved = p.sqrt();
        self.incoming.set_packet_loss(halved);
        self.outgoing.set_packet_loss(halved);
    }

    pub fn set_delay(&self, d: Duration) {
        let halved = d / 2;
        self.incoming.set_delay(halved);
        self.outgoing.set_delay(halved);
    }

    pub async fn send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()> {
        self.try_send_to(buf, target)
    }

    pub async fn recv_from(&self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)> {
        let (received_buf, remote_addr) = self
            .incoming
            .recv()
            .await
            .map_err(|_| io::ErrorKind::Other)?;

        let size = std::cmp::min(buf.len(), received_buf.len());
        buf[..size].copy_from_slice(&received_buf[..size]);

        Ok((size, remote_addr))
    }

    pub fn try_send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()> {
        self.outgoing
            .send((buf.into(), target))
            .map_err(|_| io::ErrorKind::Other)?;
        Ok(())
    }

    pub fn inner_socket(&self) -> &SocketT {
        &self.sock
    }
}

impl<SocketT: Socket, RuntimeT: Runtime> Socket
    for SimulatedNetworkConditionsSocket<SocketT, RuntimeT>
{
    type SendToFuture<'a> = impl Future<Output = io::Result<()>>;
    fn send_to<'a>(&'a self, buf: &'a [u8], target: SocketAddr) -> Self::SendToFuture<'a> {
        SimulatedNetworkConditionsSocket::send_to(self, buf, target).map(|_| Ok(()))
    }

    type RecvFromFuture<'a> = impl Future<Output = io::Result<(usize, SocketAddr)>>;
    fn recv_from<'a>(&'a self, buf: &'a mut [u8]) -> Self::RecvFromFuture<'a> {
        SimulatedNetworkConditionsSocket::recv_from(self, buf)
    }

    fn try_send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()> {
        SimulatedNetworkConditionsSocket::try_send_to(self, buf, target)
    }
}

async fn simulated_network_conditions_socket_driver<SocketT: Socket, RuntimeT: Runtime>(
    sock: Arc<SocketT>,
    shutdown: oneshot::Receiver<()>,
    outgoing: scc::Receiver<(Box<[u8]>, SocketAddr), RuntimeT>,
    incoming: scc::Sender<(Box<[u8]>, SocketAddr), RuntimeT>,
) {
    let send_loop = async {
        while let Ok((buf, target)) = outgoing.recv().await {
            let _ = sock.send_to(&buf, target).await;
            // TODO: Some send_to errors should not be ignored.
        }
    };

    let recv_loop = async {
        let mut recv_buf = vec![0; 65535].into_boxed_slice();
        loop {
            // TODO: Some recv_from errors should not be ignored.
            if let Ok((len, addr)) = sock.recv_from(&mut recv_buf).await {
                if incoming.send((recv_buf[..len].into(), addr)).is_err() {
                    return;
                }
            }
        }
    };

    select! {
        _ = recv_loop => {}
        _ = send_loop => {}
        _ = shutdown => {}
    }
}

#[cfg(test)]
mod tests {

    use runtime_trait::{
        mock_socket::{MockNetwork, MockSocket},
        tokio::tracked::TrackedTokioRuntime,
    };
    use tokio::time::Instant;

    use crate::test_utils::{drive_async_test, instant_is_approx};

    use super::*;

    type SimulatedNetworkConditionsSocket =
        super::SimulatedNetworkConditionsSocket<MockSocket, TrackedTokioRuntime>;

    async fn make_sock(
        network: &MockNetwork,
        conditions: SimulatedConditions,
    ) -> SimulatedNetworkConditionsSocket {
        let sock = network.make_sock("127.0.0.1:0".parse().unwrap()).unwrap();
        SimulatedNetworkConditionsSocket::new(sock, conditions)
    }

    #[test]
    fn test_simulated_socket() {
        drive_async_test(async {
            let network = MockNetwork::new();
            let sock1 = make_sock(
                &network,
                SimulatedConditions {
                    delay: Duration::from_secs(1),
                    packet_loss: 0.,
                },
            )
            .await;
            let addr1 = sock1.inner_socket().local_addr();
            let sock2 = make_sock(
                &network,
                SimulatedConditions {
                    delay: Duration::from_secs(1),
                    packet_loss: 0.,
                },
            )
            .await;
            let addr2 = sock2.inner_socket().local_addr();

            let start_time = Instant::now();

            let recv_task = tokio::spawn(async move {
                let mut buf = [0; 100];
                let (size, addr) = sock2.recv_from(&mut buf).await.unwrap();
                assert_eq!(addr, addr1);
                assert_eq!(&buf[0..size], b"test");
                assert!(instant_is_approx(
                    Instant::now(),
                    start_time + Duration::from_secs(1)
                ));
            });

            let data = b"test";
            sock1.send_to(data, addr2).await.unwrap();

            recv_task.await.unwrap();
        })
    }
}
