use std::{
    ops::{Deref, DerefMut},
    sync::{Arc, Weak},
};

use parking_lot::Mutex;

struct BufferPoolInner {
    buffer_size: usize,
    buffers: Vec<Box<[u8]>>,
}

impl BufferPoolInner {
    fn retake(&mut self, buffer: Box<[u8]>) {
        self.buffers.push(buffer);
    }
}

pub struct BufferPool(Arc<Mutex<BufferPoolInner>>);

impl BufferPool {
    pub fn new(buffer_size: usize) -> Self {
        Self(Arc::new(Mutex::new(BufferPoolInner {
            buffer_size,
            buffers: Vec::with_capacity(128),
        })))
    }

    pub fn get(&self) -> PooledBuffer {
        let mut inner = self.0.lock();
        let buffer = inner
            .buffers
            .pop()
            .unwrap_or_else(|| vec![0u8; inner.buffer_size].into_boxed_slice());

        PooledBuffer {
            buffer,
            pool: Arc::downgrade(&self.0),
        }
    }
}

pub struct PooledBuffer {
    buffer: Box<[u8]>,
    pool: Weak<Mutex<BufferPoolInner>>,
}

impl Deref for PooledBuffer {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.buffer
    }
}

impl DerefMut for PooledBuffer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.buffer
    }
}

impl Drop for PooledBuffer {
    fn drop(&mut self) {
        if let Some(pool) = self.pool.upgrade() {
            pool.lock().retake(std::mem::take(&mut self.buffer));
        }
    }
}

impl AsMut<[u8]> for PooledBuffer {
    fn as_mut(&mut self) -> &mut [u8] {
        &mut self.buffer
    }
}

impl AsRef<[u8]> for PooledBuffer {
    fn as_ref(&self) -> &[u8] {
        &self.buffer
    }
}

// pub struct PooledBufferSlice {
//     buffer: PooledBuffer,
//     range: Range<usize>,
// }

// impl PooledBufferSlice {
//     pub fn new(buffer: PooledBuffer, range: Range<usize>) -> Self {
//         if range.end > buffer.as_ref().len() {
//             panic!("PooledBufferSlice range is out of PooledBuffer bounds")
//         }
//         Self { buffer, range }
//     }

//     pub fn sub_slice(self, range: Range<usize>) -> Self {
//         Self::new(
//             self.buffer,
//             self.range.start + range.start..self.range.start + range.end,
//         )
//     }
// }

// impl AsMut<[u8]> for PooledBufferSlice {
//     fn as_mut(&mut self) -> &mut [u8] {
//         return &mut self.buffer[self.range.clone()];
//     }
// }

// impl AsRef<[u8]> for PooledBufferSlice {
//     fn as_ref(&self) -> &[u8] {
//         return &self.buffer[self.range.clone()];
//     }
// }

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let pool = BufferPool::new(1300);

        let buff1 = pool.get();
        let _ = pool.get();
        let _ = pool.get();

        fn test(a: impl AsRef<[u8]>) {
            println!("{}", a.as_ref().len());
        }

        test(buff1);
    }
}
