pub struct ReservedPacketNumber {
    pub pn: u64,
    pub(crate) conn_global_id: u64,
}
