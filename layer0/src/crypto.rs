use aes_gcm::{
    aead::{OsRng, Result},
    AeadInPlace, Aes256Gcm, KeyInit, Nonce, Tag,
};
use hkdf::Hkdf;
use sha2::Sha256;
use x25519_dalek::{EphemeralSecret, PublicKey, SharedSecret};

pub fn key_exchange_ephemeral(
    local_secret: EphemeralSecret,
    remote_pub_key: &PublicKey,
) -> ConnectionCipher {
    ConnectionCipher::new(&local_secret.diffie_hellman(remote_pub_key))
}

pub struct ConnectionCipher {
    enc: Aes256Gcm,
    enc_partial_iv: [u8; 4],
    dec: Aes256Gcm,
    dec_partial_iv: [u8; 4],
}

impl ConnectionCipher {
    pub fn new(shared_secret: &SharedSecret) -> Self {
        let hkdf = Hkdf::<Sha256>::new(None, shared_secret.as_bytes());

        let mut okm = [0; (32 + 4) * 2];
        // TODO: Add things info.
        hkdf.expand(b"", &mut okm).expect("Length should be valid");

        let enc = Aes256Gcm::new_from_slice(&okm[0..32]).expect("Length should be valid");
        let dec = Aes256Gcm::new_from_slice(&okm[32..64]).expect("Length should be valid");
        let mut enc_partial_iv = [0; 4];
        enc_partial_iv.copy_from_slice(&okm[64..68]);
        let mut dec_partial_iv = [0; 4];
        dec_partial_iv.copy_from_slice(&okm[68..72]);

        Self {
            enc,
            enc_partial_iv,
            dec,
            dec_partial_iv,
        }
    }

    pub fn random_cipher() -> Self {
        Self {
            enc: Aes256Gcm::new(&Aes256Gcm::generate_key(&mut OsRng)),
            dec: Aes256Gcm::new(&Aes256Gcm::generate_key(&mut OsRng)),
            enc_partial_iv: [0; 4],
            dec_partial_iv: [0; 4],
        }
    }

    /// Creates a new connection cypher with the encoding and decoding keys swapped.
    pub fn flip(self) -> Self {
        Self {
            enc: self.dec,
            enc_partial_iv: self.dec_partial_iv,
            dec: self.enc,
            dec_partial_iv: self.enc_partial_iv,
        }
    }

    pub fn decrypt_in_place_detached(
        &self,
        packet_number: u64,
        payload: &mut [u8],
        tag: &[u8],
    ) -> Result<()> {
        let nonce = Self::make_nonce(packet_number, self.dec_partial_iv);
        self.dec
            .decrypt_in_place_detached(&nonce, b"", payload, Tag::from_slice(tag))
    }

    pub fn encrypt_in_place_detached(&self, packet_number: u64, payload: &mut [u8]) -> Result<Tag> {
        let nonce = Self::make_nonce(packet_number, self.enc_partial_iv);
        self.enc.encrypt_in_place_detached(&nonce, b"", payload)
    }

    fn make_nonce(packet_number: u64, partial_iv: [u8; 4]) -> Nonce<typenum::U12> {
        let mut nonce_bytes = [0; 12];
        nonce_bytes[..8].copy_from_slice(&packet_number.to_le_bytes());
        nonce_bytes[8..].copy_from_slice(&partial_iv);
        *Nonce::<typenum::U12>::from_slice(&nonce_bytes)
    }
}
