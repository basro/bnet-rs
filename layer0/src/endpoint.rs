use std::{
    collections::{HashMap, VecDeque},
    marker::PhantomData,
    net::SocketAddr,
    ops::Deref,
    sync::{Arc, Weak},
    time::Duration,
};

use parking_lot::Mutex;
use rand::Rng;
use tokio::{
    select,
    sync::{mpsc, oneshot},
};
use x25519_dalek::{EphemeralSecret, PublicKey};

use crate::{
    buffers::{BufferPool, PooledBuffer},
    connection::{Connection, ConnectionHandle, ConnectionPublicKeys, ReceivedPayload},
    crypto::key_exchange_ephemeral,
    packet::{
        Challenge, ConnAcceptData, ConnChallengeData, ConnRejectData, ConnRequestData,
        ConnRequestFullData, Packet,
    },
    runtime::{Instant, Runtime, Socket},
    CONNECTION_CHALLENGE_DURATION, MAX_UDP_MESSAGE_LENGTH,
};

#[derive(Debug)]
pub enum ConnectionError {
    CouldntBindSocket,
    TimedOut,
    Rejected,
    Other,
}

pub struct ConnectionRequest<SocketT: Socket, RuntimeT: Runtime> {
    remote_addr: SocketAddr,
    request_id: u64,
    connection_id: u64,
    pub_key: [u8; 32],
    host: Arc<Endpoint<SocketT, RuntimeT>>,
    handled: bool,
}

impl<SocketT: Socket, RuntimeT: Runtime> ConnectionRequest<SocketT, RuntimeT> {
    pub fn accept(mut self) -> ConnectionHandle<SocketT> {
        self.handled = true;
        self.host.connection_accept(&self)
    }

    pub fn reject(mut self, code: u32) {
        self.handled = true;
        self.host.connection_reject(&self, Some(code));
    }
}

impl<SocketT: Socket, RuntimeT: Runtime> Drop for ConnectionRequest<SocketT, RuntimeT> {
    fn drop(&mut self) {
        if !self.handled {
            self.host.connection_reject(self, None);
        }
    }
}

enum IncomingConnectionState {
    Pending,
    Accepted,
    Rejected(Option<u32>),
}

struct IncomingConnections<RuntimeT: Runtime> {
    current_id: u64,
    state: HashMap<u64, IncomingConnectionState>,
    seen: VecDeque<(u64, RuntimeT::Instant)>,
    phantom: PhantomData<RuntimeT>,
}

impl<RuntimeT: Runtime> IncomingConnections<RuntimeT> {
    fn new() -> Self {
        Self {
            current_id: 0,
            state: HashMap::new(),
            seen: VecDeque::new(),
            phantom: PhantomData,
        }
    }

    /// Returns a new connection id
    fn new_id(&mut self) -> u64 {
        let id = self.current_id;
        self.current_id += 1;
        id
    }

    /// Inserts a connection_id to the state map, the connection id must not already exist.
    fn insert(&mut self, connection_id: u64, state: IncomingConnectionState) {
        self.seen
            .push_back((connection_id, RuntimeT::Instant::now()));
        assert!(self.state.insert(connection_id, state).is_none());
    }

    /// Updates the state of a connection id if the connection id exists in the map. Otherwise it does nothing
    fn update(&mut self, connection_id: u64, new_state: IncomingConnectionState) {
        use std::collections::hash_map::Entry;
        match self.state.entry(connection_id) {
            Entry::Occupied(mut entry) => {
                entry.insert(new_state);
            }
            Entry::Vacant(_) => {}
        }
    }

    fn clean_expired_ids(&mut self) {
        let seen = &mut self.seen;
        while let Some((connection_id, timestamp)) = seen.front() {
            if timestamp.elapsed() > CONNECTION_CHALLENGE_DURATION {
                self.state.remove(connection_id);
                seen.pop_front();
            } else {
                break;
            }
        }
    }
}

enum MappingState<SocketT: Socket> {
    Reserved { conn_id: u64 },
    Connected(Arc<Connection<SocketT>>),
}

impl<SocketT: Socket> MappingState<SocketT> {
    fn is_reserved_for(&self, conn_id: u64) -> bool {
        matches!(self, MappingState::Reserved {
            conn_id: reserved_conn_id,
        } if *reserved_conn_id == conn_id)
    }
}

enum OutgoingConnectionEvent {
    Challenge(ConnChallengeData),
    Rejected(ConnRejectData),
    Accepted(ConnAcceptData),
}

pub struct EndpointHandle<T: Socket, RuntimeT: Runtime>(Arc<Endpoint<T, RuntimeT>>);

impl<SocketT: Socket, RuntimeT: Runtime> Deref for EndpointHandle<SocketT, RuntimeT> {
    type Target = Endpoint<SocketT, RuntimeT>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<SocketT: Socket, RuntimeT: Runtime> From<Arc<Endpoint<SocketT, RuntimeT>>>
    for EndpointHandle<SocketT, RuntimeT>
{
    fn from(endpoint: Arc<Endpoint<SocketT, RuntimeT>>) -> Self {
        endpoint.inner_mut.lock().handle_count += 1;
        Self(endpoint)
    }
}

impl<SocketT: Socket, RuntimeT: Runtime> Clone for EndpointHandle<SocketT, RuntimeT> {
    fn clone(&self) -> Self {
        self.0.inner_mut.lock().handle_count += 1;
        Self(self.0.clone())
    }
}

impl<SocketT: Socket, RuntimeT: Runtime> Drop for EndpointHandle<SocketT, RuntimeT> {
    fn drop(&mut self) {
        self.0.inner_mut.lock().decrement_handles(&self.inner);
    }
}

pub struct Endpoint<SocketT: Socket, RuntimeT: Runtime> {
    inner_mut: Mutex<EndpointInnerMut<SocketT, RuntimeT>>,
    inner: EndpointInner<SocketT, RuntimeT>,
}

impl<SocketT: Socket, RuntimeT: Runtime> Endpoint<SocketT, RuntimeT> {
    #[allow(clippy::new_ret_no_self)]
    pub fn new(
        sock: SocketT,
    ) -> (
        EndpointHandle<SocketT, RuntimeT>,
        mpsc::Receiver<ConnectionRequest<SocketT, RuntimeT>>,
    ) {
        let challenge_key = [0u8; 16];
        let (connections_s, connections_r) =
            mpsc::channel::<ConnectionRequest<SocketT, RuntimeT>>(1);

        let (shutdown_s, shutdown_r) = oneshot::channel();

        let endpoint = Arc::new_cyclic(|me| Self {
            inner: EndpointInner {
                challenge_key,
                sock: Arc::new(sock),
                connections_s: connections_s.clone(),
                buffer_pool: BufferPool::new(MAX_UDP_MESSAGE_LENGTH),
                weak_self: me.clone(),
            },
            inner_mut: Mutex::new(EndpointInnerMut {
                mappings: HashMap::new(),
                incoming_connections: IncomingConnections::new(),
                outgoing_connections: HashMap::new(),
                handle_count: 0,
                shutdown: Some(shutdown_s),
            }),
        });

        RuntimeT::spawn(endpoint_driver(endpoint.clone(), shutdown_r));

        RuntimeT::spawn({
            let weak = Arc::downgrade(&endpoint);
            async move {
                connections_s.closed().await;
                if let Some(endpoint) = weak.upgrade() {
                    endpoint.inner_mut.lock().maybe_shutdown(&endpoint.inner);
                }
            }
        });

        (endpoint.into(), connections_r)
    }

    pub async fn connect(
        &self,
        remote_addr: SocketAddr,
    ) -> Result<ConnectionHandle<SocketT>, ConnectionError> {
        let strong_self = self
            .inner
            .weak_self
            .upgrade()
            .expect("Since self has not dropped weak_self will upgrade");
        connect(strong_self, remote_addr).await
    }

    pub fn socket(&self) -> &SocketT {
        &self.inner.sock
    }

    fn handle_datagram(&self, buffer: PooledBuffer, len: usize, remote_addr: &SocketAddr) {
        self.inner_mut
            .lock()
            .handle_datagram(&self.inner, buffer, len, remote_addr);
    }

    fn connection_accept(
        &self,
        conn_req: &ConnectionRequest<SocketT, RuntimeT>,
    ) -> ConnectionHandle<SocketT> {
        self.inner_mut
            .lock()
            .connection_accept(&self.inner, conn_req)
    }

    fn connection_reject(
        &self,
        conn_req: &ConnectionRequest<SocketT, RuntimeT>,
        code: Option<u32>,
    ) {
        self.inner_mut
            .lock()
            .connection_reject(&self.inner, conn_req, code)
    }

    fn handle_connection_closed(&self, remote_addr: &SocketAddr) {
        self.inner_mut
            .lock()
            .handle_connection_closed(&self.inner, remote_addr);
    }

    fn try_make_connection(
        &self,
        remote_addr: SocketAddr,
        for_incomming_conn_id: Option<u64>,
        local_secret: EphemeralSecret,
        public_keys: ConnectionPublicKeys,
    ) -> Result<Arc<Connection<SocketT>>, ()> {
        self.inner_mut.lock().try_make_connection(
            &self.inner,
            for_incomming_conn_id,
            remote_addr,
            local_secret,
            public_keys,
        )
    }
}

struct EndpointInner<SocketT: Socket, RuntimeT: Runtime> {
    challenge_key: [u8; 16],
    sock: Arc<SocketT>,
    connections_s: mpsc::Sender<ConnectionRequest<SocketT, RuntimeT>>,
    buffer_pool: BufferPool,
    weak_self: Weak<Endpoint<SocketT, RuntimeT>>,
}

struct EndpointInnerMut<SocketT: Socket, RuntimeT: Runtime> {
    mappings: HashMap<SocketAddr, MappingState<SocketT>>,
    incoming_connections: IncomingConnections<RuntimeT>,
    outgoing_connections: HashMap<SocketAddr, mpsc::UnboundedSender<OutgoingConnectionEvent>>,
    handle_count: usize,
    shutdown: Option<oneshot::Sender<()>>,
}

impl<SocketT: Socket, RuntimeT: Runtime> EndpointInnerMut<SocketT, RuntimeT> {
    fn handle_datagram(
        &mut self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        mut buffer: PooledBuffer,
        len: usize,
        remote_addr: &SocketAddr,
    ) {
        if let Ok(packet) = Packet::decode(&mut buffer.as_mut()[..len]) {
            use Packet::*;
            match packet {
                ConnRequest(data) => self.handle_conn_request_packet(inner, remote_addr, &data),
                ConnRequestFull(data) => {
                    self.handle_conn_request_full_packet(inner, remote_addr, &data)
                }
                Payload(data) => {
                    if let Some(MappingState::Connected(connection)) =
                        self.mappings.get(remote_addr)
                    {
                        let payload_code = data.code;
                        if let Ok((decrypted_data, packet_number)) =
                            connection.decrypt_payload(data)
                        {
                            // decrypted_data is guaranteed to be a subslice of buffer
                            // substracting the address of the buffer from the address of the decrypted data
                            // will result in the offset of decrypted_data into buffer
                            let data_addr = decrypted_data.as_ptr() as usize;
                            let data_len = decrypted_data.len();
                            let data_offset = data_addr - (buffer.as_ptr() as usize);

                            // Using this offset we are able to send the original pooled buffer and a range to the library user
                            // This avoids having to copy or allocate

                            connection.enqueue_payload_and_notify_receiver(ReceivedPayload {
                                packet_number,
                                code: payload_code,
                                buffer,
                                range: data_offset..data_offset + data_len,
                            })
                        }
                    }
                }
                ConnChallenge(data) => {
                    if let Some(events_sender) = self.outgoing_connections.get(remote_addr) {
                        let _ = events_sender.send(OutgoingConnectionEvent::Challenge(data));
                    }
                }
                ConnReject(data) => {
                    if let Some(events_sender) = self.outgoing_connections.get(remote_addr) {
                        let _ = events_sender.send(OutgoingConnectionEvent::Rejected(data));
                    }
                }
                ConnAccept(data) => {
                    if let Some(events_sender) = self.outgoing_connections.get(remote_addr) {
                        let _ = events_sender.send(OutgoingConnectionEvent::Accepted(data));
                    }
                }
            }
        }
    }

    fn handle_conn_request_packet(
        &mut self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        remote_addr: &SocketAddr,
        _conn_req_data: &ConnRequestData,
    ) {
        if inner.connections_s.is_closed() {
            return;
        }
        // If a mapping exists we can ignore ConnRequest packets from this address
        if self.mappings.contains_key(remote_addr) {
            return;
        }

        let connection_id = self.incoming_connections.new_id();

        let mut buf = inner.buffer_pool.get();
        let packet = Packet::ConnChallenge(ConnChallengeData {
            challenge: Challenge::new(connection_id, remote_addr, &inner.challenge_key),
        });
        let len = packet.encode(buf.as_mut()).unwrap();
        let _ = inner.sock.try_send_to(&buf[..len], *remote_addr);
    }

    fn handle_conn_request_full_packet(
        &mut self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        remote_addr: &SocketAddr,
        data: &ConnRequestFullData,
    ) {
        // If the challenge data is not valid then we ignore the packet.
        if !data.challenge.validate(remote_addr, &inner.challenge_key) {
            return;
        }

        let connection_id = data.challenge.connection_id;
        self.incoming_connections.clean_expired_ids();

        match self.incoming_connections.state.get(&connection_id) {
            None => {
                // This connection id is new to us
                if self.mappings.contains_key(remote_addr) {
                    // The remote address is already mapped.
                    // We wont allow new connections until this mapping is cleared
                    self.send_reject_to(
                        inner,
                        remote_addr,
                        ConnRejectData {
                            request_id: data.request_id,
                            code: 0,
                        },
                    );
                    return;
                }

                let connection_request = ConnectionRequest {
                    request_id: data.request_id,
                    connection_id,
                    remote_addr: *remote_addr,
                    pub_key: data.pub_key,
                    host: inner.weak_self.upgrade().unwrap(),
                    handled: false,
                };
                if inner.connections_s.try_send(connection_request).is_err() {
                    // The app seems busy so we reject new connections
                    self.send_reject_to(
                        inner,
                        remote_addr,
                        ConnRejectData {
                            request_id: data.request_id,
                            code: 0,
                        },
                    );
                } else {
                    self.mappings.insert(
                        *remote_addr,
                        MappingState::Reserved {
                            conn_id: connection_id,
                        },
                    );
                    self.incoming_connections
                        .insert(connection_id, IncomingConnectionState::Pending);
                }
            }

            Some(IncomingConnectionState::Accepted) => {
                if let Some(MappingState::Connected(connection)) = self.mappings.get(remote_addr) {
                    let mut buf = inner.buffer_pool.get();
                    let packet = Packet::ConnAccept(ConnAcceptData {
                        request_id: data.request_id,
                        pub_key: connection.public_keys().local.to_bytes(),
                    });
                    let len = packet.encode(buf.as_mut()).unwrap();
                    let _ = inner.sock.try_send_to(&buf[..len], *remote_addr);
                }
            }

            Some(IncomingConnectionState::Rejected(code)) => {
                // The connection request id has already been marked as rejected
                // so we resend the rejection message
                if let Some(code) = code {
                    self.send_reject_to(
                        inner,
                        remote_addr,
                        ConnRejectData {
                            request_id: data.request_id,
                            code: *code,
                        },
                    );
                }
            }

            Some(IncomingConnectionState::Pending) => {
                // Connection is waiting for app to decide so ignore this packet.
            }
        }
    }

    fn connection_accept(
        &mut self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        conn_req: &ConnectionRequest<SocketT, RuntimeT>,
    ) -> ConnectionHandle<SocketT> {
        let private_key = EphemeralSecret::new(rand::thread_rng());
        let pub_key = PublicKey::from(&private_key);

        let connection = self
            .try_make_connection(
                inner,
                Some(conn_req.connection_id),
                conn_req.remote_addr,
                private_key,
                ConnectionPublicKeys {
                    remote: PublicKey::from(conn_req.pub_key),
                    local: pub_key,
                },
            )
            .expect("Connection mapping should have been reserved and not fail here");

        // Change the state of the connection_id to Accepted
        self.incoming_connections
            .update(conn_req.connection_id, IncomingConnectionState::Accepted);

        {
            // Send the accept message
            let mut buf = inner.buffer_pool.get();
            let packet = Packet::ConnAccept(ConnAcceptData {
                request_id: conn_req.request_id,
                pub_key: pub_key.to_bytes(),
            });
            let len = packet.encode(buf.as_mut()).unwrap();
            let _ = inner.sock.try_send_to(&buf[..len], conn_req.remote_addr);
        }

        connection.into()
    }

    fn connection_reject(
        &mut self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        conn_req: &ConnectionRequest<SocketT, RuntimeT>,
        code: Option<u32>,
    ) {
        self.incoming_connections.update(
            conn_req.connection_id,
            IncomingConnectionState::Rejected(code),
        );

        assert!(
            self.mappings
                .remove(&conn_req.remote_addr)
                .expect("Rejected connection requests should have a previously existing mapping")
                .is_reserved_for(conn_req.connection_id),
            "Rejected connection requests previous mapping should be reserved for the connection id"
        );

        if let Some(code) = code {
            self.send_reject_to(
                inner,
                &conn_req.remote_addr,
                ConnRejectData {
                    request_id: conn_req.request_id,
                    code,
                },
            );
        }

        self.maybe_shutdown(inner);
    }

    fn send_reject_to(
        &self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        remote_addr: &SocketAddr,
        data: ConnRejectData,
    ) {
        let mut buf = inner.buffer_pool.get();
        let packet = Packet::ConnReject(data);
        let len = packet.encode(buf.as_mut()).unwrap();
        let _ = inner.sock.try_send_to(&buf[..len], *remote_addr);
    }

    fn handle_connection_closed(
        &mut self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        remote_addr: &SocketAddr,
    ) {
        self.mappings.remove(remote_addr);
        self.maybe_shutdown(inner);
    }

    fn try_make_connection(
        &mut self,
        inner: &EndpointInner<SocketT, RuntimeT>,
        for_incomming_conn_id: Option<u64>,
        remote_addr: SocketAddr,
        local_secret: EphemeralSecret,
        public_keys: ConnectionPublicKeys,
    ) -> Result<Arc<Connection<SocketT>>, ()> {
        use std::collections::hash_map::Entry;
        let mappings_entry = self.mappings.entry(remote_addr);

        // Make sure that we don't overwrite a previously existing mapping of this remote_addr
        if let Entry::Occupied(entry) = &mappings_entry {
            match entry.get() {
                MappingState::Reserved {
                    conn_id: reserved_conn_id,
                } => {
                    if for_incomming_conn_id
                        .filter(|id| id == reserved_conn_id)
                        .is_none()
                    {
                        return Err(());
                    }
                }
                MappingState::Connected(_) => return Err(()),
            }
        }

        let mut cipher = key_exchange_ephemeral(local_secret, &public_keys.remote);
        if for_incomming_conn_id.is_none() {
            // The key exchange results in the same cipher pair for both peers.
            // Outgoing connections flip the cipher pair while incomming connections leave it as is.
            cipher = cipher.flip();
        }

        let (on_closed, on_closed_r) = oneshot::channel();
        let conn = Connection::new(
            inner.sock.clone(),
            remote_addr,
            cipher,
            on_closed,
            public_keys,
        );

        // Spawn a task that will wait for the connection to close and call handle_connection_closed
        RuntimeT::spawn({
            let weak_self = inner.weak_self.clone();
            async move {
                let _ = on_closed_r.await;
                if let Some(endpoint) = weak_self.upgrade() {
                    endpoint.handle_connection_closed(&remote_addr);
                }
            }
        });

        let new_mapping = MappingState::Connected(conn.clone());

        // Insert the connection into the mappings.
        match mappings_entry {
            Entry::Occupied(mut entry) => {
                entry.insert(new_mapping);
            }
            Entry::Vacant(entry) => {
                entry.insert(new_mapping);
            }
        };

        Ok(conn)
    }

    fn decrement_handles(&mut self, inner: &EndpointInner<SocketT, RuntimeT>) {
        self.handle_count -= 1;
        self.maybe_shutdown(inner);
    }

    fn maybe_shutdown(&mut self, inner: &EndpointInner<SocketT, RuntimeT>) {
        if self.handle_count == 0 && self.mappings.is_empty() && inner.connections_s.is_closed() {
            if let Some(s) = self.shutdown.take() {
                let _ = s.send(());
            }
        }
    }
}

async fn recv_from_and_ignore_errors<SocketT: Socket>(
    sock: &SocketT,
    buf: &mut [u8],
) -> (usize, SocketAddr) {
    loop {
        match sock.recv_from(buf).await {
            Err(_) => {
                continue;
            }
            Ok(val) => {
                return val;
            }
        }
    }
}

async fn endpoint_driver<SocketT: Socket, RuntimeT: Runtime>(
    endpoint: Arc<Endpoint<SocketT, RuntimeT>>,
    shutdown: oneshot::Receiver<()>,
) {
    let recv_loop = async {
        loop {
            let mut pooled_buffer = endpoint.inner.buffer_pool.get();

            let (len, remote_addr) =
                recv_from_and_ignore_errors(endpoint.inner.sock.as_ref(), pooled_buffer.as_mut())
                    .await;

            endpoint.handle_datagram(pooled_buffer, len, &remote_addr);
        }
    };

    select! {
        _ = recv_loop => { unreachable!() }
        _ = shutdown => {}
    };
}

struct Defer<F: FnOnce()>(Option<F>);

impl<F: FnOnce()> Drop for Defer<F> {
    fn drop(&mut self) {
        if let Some(f) = self.0.take() {
            f()
        }
    }
}

/// Defer execution of a closure until the return value is dropped.
fn defer<F: FnOnce()>(f: F) -> impl Drop {
    Defer(Some(f))
}

async fn connect<SocketT: Socket, RuntimeT: Runtime>(
    endpoint: Arc<Endpoint<SocketT, RuntimeT>>,
    remote_addr: SocketAddr,
) -> Result<ConnectionHandle<SocketT>, ConnectionError> {
    let (events_s, mut events_r) = mpsc::unbounded_channel();

    {
        let mut lock = endpoint.inner_mut.lock();
        if lock.outgoing_connections.contains_key(&remote_addr) {
            return Err(ConnectionError::CouldntBindSocket);
        }

        lock.outgoing_connections.insert(remote_addr, events_s);
    }

    let _defer = defer(|| {
        endpoint
            .inner_mut
            .lock()
            .outgoing_connections
            .remove(&remote_addr);
    });

    let send_buf = &mut [0; 2048];
    let private_key = EphemeralSecret::new(rand::thread_rng());
    let pub_key = PublicKey::from(&private_key);
    let sock = &endpoint.inner.sock;

    let request_id: u64 = rand::thread_rng().gen();

    let send_conn_req_messages = async {
        loop {
            let size = Packet::ConnRequest(ConnRequestData {
                version: 1,
                request_id,
            })
            .encode(send_buf)
            .unwrap();
            sock.send_to(&send_buf[..size], remote_addr).await.unwrap();
            RuntimeT::sleep(Duration::from_millis(250)).await;
        }
    };

    let wait_for_challenge = async {
        while let Some(event) = events_r.recv().await {
            use OutgoingConnectionEvent::*;
            match event {
                Challenge(data) => return Ok(data),
                Rejected(data) if data.request_id == request_id => {
                    return Err(ConnectionError::Rejected)
                }
                _ => {}
            }
        }
        Err(ConnectionError::Other)
    };

    let challenge: ConnChallengeData = select! {
        _ = send_conn_req_messages => { unreachable!("this future should never finish"); }

        response = wait_for_challenge => response?,

        _ = RuntimeT::sleep(Duration::from_secs(10)) => {
            return Err(ConnectionError::TimedOut);
        }
    };

    let send_conn_req_full_messages = async {
        loop {
            let packet = Packet::ConnRequestFull(ConnRequestFullData {
                challenge: challenge.challenge,
                request_id,
                pub_key: pub_key.to_bytes(),
            });
            let size = packet.encode(send_buf).unwrap();
            sock.send_to(&send_buf[..size], remote_addr).await.unwrap();
            RuntimeT::sleep(Duration::from_millis(250)).await;
        }
    };

    let wait_for_accept = async {
        while let Some(event) = events_r.recv().await {
            use OutgoingConnectionEvent::*;
            match event {
                Accepted(data) if data.request_id == request_id => return Ok(data),
                Rejected(data) if data.request_id == request_id => {
                    return Err(ConnectionError::Rejected)
                }
                _ => {}
            }
        }
        Err(ConnectionError::Other)
    };

    let accept_data = select! {
        _ = send_conn_req_full_messages => { unreachable!("this future should never finish"); }

        response = wait_for_accept => { response? }

        _ = RuntimeT::sleep(CONNECTION_CHALLENGE_DURATION) => {
            return Err(ConnectionError::TimedOut);
        }
    };

    let connection = endpoint
        .try_make_connection(
            remote_addr,
            None,
            private_key,
            ConnectionPublicKeys {
                remote: PublicKey::from(accept_data.pub_key),
                local: pub_key,
            },
        )
        .map_err(|_| ConnectionError::Other)?;

    Ok(connection.into())
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use runtime_trait::{
        mock_socket::{MockNetwork, MockSocket},
        tokio::tracked::TrackedTokioRuntime,
    };
    use tokio::sync::mpsc;

    use crate::{socket::SimulatedConditions, test_utils::drive_async_test};

    type SimulatedNetworkConditionsSocket =
        crate::socket::SimulatedNetworkConditionsSocket<MockSocket, TrackedTokioRuntime>;

    type Endpoint = super::Endpoint<SimulatedNetworkConditionsSocket, TrackedTokioRuntime>;
    type EndpointHandle =
        super::EndpointHandle<SimulatedNetworkConditionsSocket, TrackedTokioRuntime>;
    type ConnectionRequest =
        super::ConnectionRequest<SimulatedNetworkConditionsSocket, TrackedTokioRuntime>;

    async fn make_ep(
        network: &MockNetwork,
        conditions: SimulatedConditions,
    ) -> (EndpointHandle, mpsc::Receiver<ConnectionRequest>) {
        let sock = SimulatedNetworkConditionsSocket::new(
            network.make_sock("127.0.0.1:0".parse().unwrap()).unwrap(),
            conditions,
        );
        Endpoint::new(sock)
    }

    #[test]
    fn test_it_connects() {
        drive_async_test(async {
            let network = MockNetwork::new();
            let (host, mut conns) = make_ep(
                &network,
                SimulatedConditions {
                    delay: Duration::from_secs(1),
                    packet_loss: 0.2,
                },
            )
            .await;

            let host_addr = host.socket().inner_socket().local_addr();

            let (client, _) = make_ep(
                &network,
                SimulatedConditions {
                    delay: Duration::from_secs(1),
                    packet_loss: 0.,
                },
            )
            .await;

            let client_conn = tokio::spawn(async move {
                client
                    .connect(host_addr)
                    .await
                    .expect("connection should succeed")
            });

            let host_conn = tokio::spawn(async move { conns.recv().await.unwrap().accept() });

            let client_conn = client_conn.await.unwrap();
            let host_conn = host_conn.await.unwrap();

            assert_eq!(
                client_conn.public_keys().remote,
                host_conn.public_keys().local
            );

            assert_eq!(
                client_conn.public_keys().local,
                host_conn.public_keys().remote
            );
        });
    }
}
