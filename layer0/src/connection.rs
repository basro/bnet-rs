use std::{
    collections::VecDeque,
    fmt,
    marker::PhantomData,
    net::SocketAddr,
    ops::{Deref, Range},
    sync::Arc,
};

use once_cell::sync::Lazy;
use parking_lot::Mutex;
use tokio::sync::{oneshot, Notify};
use x25519_dalek::PublicKey;

use crate::{
    buffers::{BufferPool, PooledBuffer},
    packet_number::ReservedPacketNumber,
};

use crate::{
    crypto::ConnectionCipher,
    packet::{encode_payload_packet_in_place, PayloadData},
    received_packets_set::{ReceivedPacketState, ReceivedPacketsSet},
    runtime::Socket,
    MAX_PAYLOAD_HEADER_SIZE, MAX_UDP_MESSAGE_LENGTH,
};

static BUFFER_POOL: Lazy<BufferPool> = Lazy::new(|| BufferPool::new(MAX_UDP_MESSAGE_LENGTH));

#[derive(Debug)]
pub struct ConnectionHandle<SocketT: Socket>(Arc<Connection<SocketT>>);

impl<SocketT: Socket> Deref for ConnectionHandle<SocketT> {
    type Target = Connection<SocketT>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<SocketT: Socket> From<Arc<Connection<SocketT>>> for ConnectionHandle<SocketT> {
    fn from(conn: Arc<Connection<SocketT>>) -> Self {
        conn.inner_mut.lock().handle_count += 1;
        Self(conn)
    }
}

impl<SocketT: Socket> Clone for ConnectionHandle<SocketT> {
    fn clone(&self) -> Self {
        self.0.inner_mut.lock().handle_count += 1;
        Self(self.0.clone())
    }
}

impl<SocketT: Socket> Drop for ConnectionHandle<SocketT> {
    fn drop(&mut self) {
        self.0.inner_mut.lock().decrement_handles();
    }
}

pub enum TryRecvError {
    Empty,
    End,
}

pub struct ConnectionPublicKeys {
    pub remote: PublicKey,
    pub local: PublicKey,
}

pub struct ReceivedPayload {
    pub packet_number: u64,
    pub code: u8,
    pub(crate) buffer: PooledBuffer,
    pub(crate) range: Range<usize>,
}

impl ReceivedPayload {
    pub fn data(&self) -> &[u8] {
        &self.buffer[self.range.clone()]
    }
}

struct ConnectionInner<SocketT: Socket> {
    sock: Arc<SocketT>,
    remote_addr: SocketAddr,
    cipher: ConnectionCipher,
    notify_receiver: Notify,
    max_receiver_queue_len: usize,
    public_keys: ConnectionPublicKeys,
    global_id: u64,
}

struct ConnectionInnerMut<SocketT: Socket> {
    outgoing_packet_number: u64,
    largest_ack_received: u64,
    received_packets: ReceivedPacketsSet,
    receiver_queue: VecDeque<ReceivedPayload>,
    handle_count: usize,
    on_closed: Option<oneshot::Sender<()>>,
    phantom: PhantomData<SocketT>,
}

impl<SocketT: Socket> ConnectionInnerMut<SocketT> {
    fn try_send_in_place(
        &mut self,
        inner: &ConnectionInner<SocketT>,
        code: u8,
        data: &mut [u8],
        offset: usize,
    ) -> std::io::Result<u64> {
        let packet_number = self.outgoing_packet_number;
        let encoded_start_pos = encode_payload_packet_in_place(
            code,
            packet_number,
            self.largest_ack_received,
            &inner.cipher,
            data,
            offset,
        );

        inner
            .sock
            .try_send_to(&data[encoded_start_pos..], inner.remote_addr)?;

        // The send was successful so we increment the packet number
        // If the send has failed it is fine to use the same outgoing packet number again.
        self.outgoing_packet_number += 1;

        Ok(packet_number)
    }

    fn try_send(
        &mut self,
        inner: &ConnectionInner<SocketT>,
        code: u8,
        data: &[u8],
    ) -> std::io::Result<u64> {
        let mut send_buf = BUFFER_POOL.get();
        let buf = &mut send_buf[..MAX_PAYLOAD_HEADER_SIZE + data.len()];
        buf[MAX_PAYLOAD_HEADER_SIZE..].copy_from_slice(data);

        self.try_send_in_place(inner, code, buf, MAX_PAYLOAD_HEADER_SIZE)
    }

    fn is_closed(&self) -> bool {
        self.on_closed.is_none()
    }

    fn decrypt_payload<'data>(
        &mut self,
        inner: &ConnectionInner<SocketT>,
        data: PayloadData<'data>,
    ) -> Result<(&'data [u8], u64), ()> {
        if self.is_closed() {
            return Err(());
        }

        let packet_number = data.expand_packet_number(self.received_packets.get_next_expected_pn());

        if self.received_packets.get(packet_number) != ReceivedPacketState::Pending {
            // Packet was either already received or the packet number is too old so
            // we ignore this packet.
            return Err(());
        }

        let buf = data.decrypt(&inner.cipher, packet_number)?;

        self.received_packets.set(packet_number);

        Ok((buf, packet_number))
    }

    fn enqueue_payload_and_notify_receiver(
        &mut self,
        inner: &ConnectionInner<SocketT>,
        payload: ReceivedPayload,
    ) {
        if self.receiver_queue.len() >= inner.max_receiver_queue_len {
            self.receiver_queue.pop_front();
        }

        self.receiver_queue.push_back(payload);
        inner.notify_receiver.notify_one();
    }

    fn try_recv(&mut self) -> Result<ReceivedPayload, TryRecvError> {
        if let Some(data) = self.receiver_queue.pop_back() {
            Ok(data)
        } else if self.is_closed() {
            Err(TryRecvError::End)
        } else {
            Err(TryRecvError::Empty)
        }
    }

    fn decrement_handles(&mut self) {
        self.handle_count -= 1;
        if self.handle_count == 0 {
            if let Some(on_closed) = self.on_closed.take() {
                let _ = on_closed.send(());
            }
        }
    }

    fn set_largest_acked_packet(
        &mut self,
        packet_number: u64,
    ) -> Result<(), SetLargestAckedPacketError> {
        if self.outgoing_packet_number <= packet_number {
            // Outgoing packet number hasn't been sent used yet, so it should not be possible
            // for it to be acked already
            return Err(SetLargestAckedPacketError);
        }
        self.largest_ack_received = std::cmp::max(packet_number, self.largest_ack_received);
        Ok(())
    }

    fn reserve_packet_number(&mut self, inner: &ConnectionInner<SocketT>) -> ReservedPacketNumber {
        let pn = self.outgoing_packet_number;
        self.outgoing_packet_number += 1;
        ReservedPacketNumber {
            pn,
            conn_global_id: inner.global_id,
        }
    }
}

pub struct Connection<SocketT: Socket> {
    inner_mut: Mutex<ConnectionInnerMut<SocketT>>,
    inner: ConnectionInner<SocketT>,
}

impl<SocketT: Socket> std::fmt::Debug for Connection<SocketT> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Connection")
            /*.field("outgoing_packet_number", &self.data.outgoing_packet_number)
            .field("received_packets", &self.received_packets)
            .field("remote_addr", &self.remote_addr)*/
            .finish()
    }
}

#[derive(Debug)]
pub struct SetLargestAckedPacketError;

impl fmt::Display for SetLargestAckedPacketError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Largest acked packet number cannot be larger than the largest sent packet number"
        )
    }
}

impl std::error::Error for SetLargestAckedPacketError {}

static GLOBAL_ID_COUNTER: Mutex<u64> = Mutex::new(0);
fn new_global_id() -> u64 {
    let mut counter = GLOBAL_ID_COUNTER.lock();
    let new_id = *counter;
    *counter += 1;
    new_id
}

impl<SocketT: Socket> Connection<SocketT> {
    pub fn new(
        sock: Arc<SocketT>,
        remote_addr: SocketAddr,
        cipher: ConnectionCipher,
        on_closed: oneshot::Sender<()>,
        public_keys: ConnectionPublicKeys,
    ) -> Arc<Self> {
        let max_receiver_queue_len = 5;

        Arc::new(Self {
            inner_mut: Mutex::new(ConnectionInnerMut {
                outgoing_packet_number: 1,
                largest_ack_received: 0,
                received_packets: ReceivedPacketsSet::new(),
                receiver_queue: VecDeque::with_capacity(max_receiver_queue_len),
                handle_count: 0,
                on_closed: Some(on_closed),
                phantom: PhantomData,
            }),
            inner: ConnectionInner {
                sock,
                remote_addr,
                cipher,
                notify_receiver: Notify::new(),
                max_receiver_queue_len,
                public_keys,
                global_id: new_global_id(),
            },
        })
    }

    pub fn set_largest_ack_received(
        &self,
        packet_number: u64,
    ) -> Result<(), SetLargestAckedPacketError> {
        self.inner_mut
            .lock()
            .set_largest_acked_packet(packet_number)
    }

    pub fn reserve_packet_number(&self) -> ReservedPacketNumber {
        self.inner_mut.lock().reserve_packet_number(&self.inner)
    }

    pub fn try_send(&self, code: u8, data: &[u8]) -> std::io::Result<u64> {
        self.inner_mut.lock().try_send(&self.inner, code, data)
    }

    pub fn try_send_in_place(
        &self,
        code: u8,
        data: &mut [u8],
        offset: usize,
    ) -> std::io::Result<u64> {
        self.inner_mut
            .lock()
            .try_send_in_place(&self.inner, code, data, offset)
    }

    pub async fn send(&self, code: u8, data: &[u8]) -> std::io::Result<u64> {
        let rpn = self.reserve_packet_number();
        self.send_pn(rpn, code, data).await
    }

    pub async fn send_in_place(
        &self,
        code: u8,
        data: &mut [u8],
        offset: usize,
    ) -> std::io::Result<u64> {
        let rpn = self.reserve_packet_number();
        self.send_pn_in_place(rpn, code, data, offset).await
    }

    pub async fn send_pn(
        &self,
        reserved_pn: ReservedPacketNumber,
        code: u8,
        data: &[u8],
    ) -> std::io::Result<u64> {
        let mut send_buf = BUFFER_POOL.get();
        let buf = &mut send_buf[..MAX_PAYLOAD_HEADER_SIZE + data.len()];
        buf[MAX_PAYLOAD_HEADER_SIZE..].copy_from_slice(data);

        self.send_pn_in_place(reserved_pn, code, buf, MAX_PAYLOAD_HEADER_SIZE)
            .await
    }

    pub async fn send_pn_in_place(
        &self,
        reserved_pn: ReservedPacketNumber,
        code: u8,
        data: &mut [u8],
        offset: usize,
    ) -> std::io::Result<u64> {
        assert!(
            reserved_pn.conn_global_id == self.inner.global_id,
            "Packet number was not reserved from this connection"
        );

        let packet_number = reserved_pn.pn;
        let encoded_start_pos = encode_payload_packet_in_place(
            code,
            packet_number,
            self.inner_mut.lock().largest_ack_received,
            &self.inner.cipher,
            data,
            offset,
        );

        self.inner
            .sock
            .send_to(&data[encoded_start_pos..], self.inner.remote_addr)
            .await?;

        Ok(packet_number)
    }

    pub async fn recv(&self) -> Option<ReceivedPayload> {
        loop {
            match self.inner_mut.lock().try_recv() {
                Ok(bytes) => return Some(bytes),
                Err(TryRecvError::End) => return None,
                Err(TryRecvError::Empty) => {}
            }

            self.inner.notify_receiver.notified().await;
        }
    }

    pub fn public_keys(&self) -> &ConnectionPublicKeys {
        &self.inner.public_keys
    }

    pub fn socket(&self) -> &SocketT {
        &self.inner.sock
    }

    pub(crate) fn decrypt_payload<'data>(
        &self,
        data: PayloadData<'data>,
    ) -> Result<(&'data [u8], u64), ()> {
        self.inner_mut.lock().decrypt_payload(&self.inner, data)
    }

    pub(crate) fn enqueue_payload_and_notify_receiver(&self, payload: ReceivedPayload) {
        self.inner_mut
            .lock()
            .enqueue_payload_and_notify_receiver(&self.inner, payload)
    }
}
