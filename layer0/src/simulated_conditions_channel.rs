use parking_lot::Mutex;
use rand::Rng;
use runtime_trait::{Instant, Runtime, RuntimeExt};
use tokio::sync::Notify;

use std::collections::VecDeque;
use std::fmt::Display;
use std::sync::Arc;
use std::time::Duration;

#[derive(Copy, Clone, Default)]
pub struct SimulatedConditions {
    pub delay: Duration,
    pub packet_loss: f32,
}

impl SimulatedConditions {
    pub(crate) fn halve(self) -> Self {
        Self {
            delay: self.delay / 2,
            packet_loss: self.packet_loss.sqrt(),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum TryRecvError {
    Empty,
    Closed,
}

impl std::error::Error for TryRecvError {}

impl Display for TryRecvError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Empty => write!(f, "channel is empty"),
            Self::Closed => write!(f, "channel is empty and closed"),
        }
    }
}

pub struct Sender<T: Send + 'static, RuntimeT: Runtime>(
    Arc<SimulatedConditionsChannel<T, RuntimeT>>,
);

impl<T: Send + 'static, RuntimeT: Runtime> Sender<T, RuntimeT> {
    pub fn send(&self, msg: T) -> Result<(), T> {
        self.0.send(msg)
    }

    pub fn conditions(&self) -> SimulatedConditions {
        self.0.conditions()
    }

    pub fn set_delay(&self, delay: Duration) {
        self.0.set_delay(delay);
    }

    pub fn set_packet_loss(&self, packet_loss: f32) {
        self.0.set_packet_loss(packet_loss);
    }
}

impl<T: Send + 'static, RuntimeT: Runtime> Clone for Sender<T, RuntimeT> {
    fn clone(&self) -> Self {
        self.0.inner.lock().num_senders += 1;
        Self(self.0.clone())
    }
}

impl<T: Send + 'static, RuntimeT: Runtime> Drop for Sender<T, RuntimeT> {
    fn drop(&mut self) {
        self.0.decrement_senders()
    }
}

pub struct Receiver<T: Send + 'static, RuntimeT: Runtime>(
    Arc<SimulatedConditionsChannel<T, RuntimeT>>,
);

impl<T: Send + 'static, RuntimeT: Runtime> Receiver<T, RuntimeT> {
    pub fn try_recv(&self) -> Result<T, TryRecvError> {
        self.0.try_recv()
    }

    pub async fn recv(&self) -> Result<T, ()> {
        self.0.recv().await
    }

    pub fn conditions(&self) -> SimulatedConditions {
        self.0.conditions()
    }

    pub fn set_delay(&self, delay: Duration) {
        self.0.set_delay(delay);
    }

    pub fn set_packet_loss(&self, packet_loss: f32) {
        self.0.set_packet_loss(packet_loss);
    }
}

impl<T: Send + 'static, RuntimeT: Runtime> Clone for Receiver<T, RuntimeT> {
    fn clone(&self) -> Self {
        self.0.inner.lock().num_receivers += 1;
        Self(self.0.clone())
    }
}

impl<T: Send + 'static, RuntimeT: Runtime> Drop for Receiver<T, RuntimeT> {
    fn drop(&mut self) {
        self.0.decrement_receivers()
    }
}

pub struct SimulatedConditionsChannel<T: Send + 'static, RuntimeT: Runtime> {
    inner: Mutex<SimulatedConditionsChannelInner<T, RuntimeT>>,
    notify_recv: Notify,
    notify_driver: Notify,
}

struct SimulatedConditionsChannelInner<T, RuntimeT: Runtime> {
    delay_queue: VecDeque<(T, RuntimeT::Instant)>,
    recv_queue: VecDeque<T>,
    conditions: SimulatedConditions,
    num_receivers: usize,
    num_senders: usize,
}

enum ProcessDelayError {
    Empty,
    Closed,
    NoReceivers,
}

impl<T: Send + 'static, RuntimeT: Runtime> SimulatedConditionsChannel<T, RuntimeT> {
    #[allow(clippy::new_ret_no_self)]
    pub fn new(conditions: SimulatedConditions) -> (Sender<T, RuntimeT>, Receiver<T, RuntimeT>) {
        let this = Arc::new(SimulatedConditionsChannel {
            inner: Mutex::new(SimulatedConditionsChannelInner {
                delay_queue: VecDeque::new(),
                recv_queue: VecDeque::new(),
                conditions,
                num_receivers: 1,
                num_senders: 1,
            }),
            notify_recv: Notify::new(),
            notify_driver: Notify::new(),
        });
        RuntimeT::spawn(simulated_conditions_channel_driver(this.clone()));
        (Sender(this.clone()), Receiver(this))
    }

    fn send(&self, msg: T) -> Result<(), T> {
        let mut inner = self.inner.lock();

        if inner.num_receivers == 0 {
            return Err(msg);
        }

        if rand::thread_rng().gen::<f32>() >= inner.conditions.packet_loss {
            inner.delay_queue.push_back((msg, RuntimeT::Instant::now()));

            drop(inner);
            self.notify_driver.notify_waiters();
        }
        Ok(())
    }

    fn try_recv(&self) -> Result<T, TryRecvError> {
        let mut inner = self.inner.lock();
        let popped = inner.recv_queue.pop_front();
        #[allow(clippy::unnecessary_lazy_evaluations)]
        popped.ok_or_else(|| {
            if inner.num_senders == 0 && inner.delay_queue.is_empty() {
                TryRecvError::Closed
            } else {
                TryRecvError::Empty
            }
        })
    }

    async fn recv(&self) -> Result<T, ()> {
        let future = self.notify_recv.notified();
        tokio::pin!(future);

        loop {
            future.as_mut().enable();

            match self.try_recv() {
                Ok(msg) => return Ok(msg),
                Err(TryRecvError::Closed) => return Err(()),
                Err(TryRecvError::Empty) => {}
            }
            future.as_mut().await;
            future.set(self.notify_recv.notified());
        }
    }

    fn conditions(&self) -> SimulatedConditions {
        self.inner.lock().conditions
    }

    fn set_delay(&self, delay: Duration) {
        self.inner.lock().conditions.delay = delay;
        self.notify_driver.notify_waiters();
    }

    fn set_packet_loss(&self, packet_loss: f32) {
        self.inner.lock().conditions.packet_loss = packet_loss;
    }

    fn process_delay_queue(&self) -> Result<RuntimeT::Instant, ProcessDelayError> {
        let mut inner = self.inner.lock();

        if inner.num_receivers == 0 {
            return Err(ProcessDelayError::NoReceivers);
        }

        let mut should_notify = false;
        loop {
            let next_arrival = inner
                .delay_queue
                .front()
                .map(|(_, time)| *time + inner.conditions.delay);
            if next_arrival
                .filter(|time| *time <= Instant::now())
                .is_some()
            {
                let (msg, _) = inner.delay_queue.pop_front().unwrap(); // The front was not None so pop_front is guaranteed to be Some(_).
                inner.recv_queue.push_back(msg);
                should_notify = true;
            } else {
                #[allow(clippy::unnecessary_lazy_evaluations)]
                let result = next_arrival.ok_or_else(|| {
                    if inner.num_senders == 0 {
                        ProcessDelayError::Closed
                    } else {
                        ProcessDelayError::Empty
                    }
                });
                drop(inner);
                if should_notify {
                    self.notify_recv.notify_one()
                }
                return result;
            }
        }
    }

    fn decrement_receivers(&self) {
        let mut inner = self.inner.lock();
        inner.num_receivers -= 1;
        if inner.num_receivers == 0 && inner.delay_queue.is_empty() {
            drop(inner);
            self.notify_driver.notify_waiters();
        }
    }

    fn decrement_senders(&self) {
        let mut inner = self.inner.lock();
        inner.num_senders -= 1;
        if inner.num_senders == 0 {
            self.notify_driver.notify_waiters()
        }
    }
}

async fn simulated_conditions_channel_driver<T: Send + 'static, RuntimeT: Runtime>(
    chan: Arc<SimulatedConditionsChannel<T, RuntimeT>>,
) {
    loop {
        let delay_notified = chan.notify_driver.notified();

        loop {
            match chan.process_delay_queue() {
                Ok(deadline) => {
                    RuntimeT::sleep_until(deadline).await;
                }
                Err(ProcessDelayError::NoReceivers) => {
                    // If there are no receivers there's no reason to continue the driver
                    return;
                }
                Err(ProcessDelayError::Closed) => {
                    // We notify all receivers (not just notify_one)
                    chan.notify_recv.notify_waiters();
                    return;
                }
                Err(ProcessDelayError::Empty) => {
                    break;
                }
            }
        }

        delay_notified.await;
    }
}

#[cfg(test)]
mod tests {
    use crate::test_utils::drive_async_test;

    use super::*;
    use runtime_trait::tokio::tracked::TrackedTokioRuntime;
    use tokio::time::Instant;

    type SimulatedConditionsChannel<T> = super::SimulatedConditionsChannel<T, TrackedTokioRuntime>;

    fn instant_is_approx(a: Instant, b: Instant) -> bool {
        let max = Duration::from_millis(50);
        a - b < max && b - a < max
    }

    async fn sleep_secs(secs: u64) {
        tokio::time::sleep(Duration::from_secs(secs)).await
    }

    #[test]
    fn test_delay() {
        drive_async_test(async {
            let delay = Duration::from_secs(10);
            let (s, r) = SimulatedConditionsChannel::new(SimulatedConditions {
                delay: Duration::from_secs(10),
                packet_loss: 0.,
            });

            let count = 6;
            tokio::spawn(async move {
                for _ in 0..count {
                    s.send(Instant::now() + delay).unwrap();
                    sleep_secs(1).await;
                }
            });

            while let Ok(expected) = r.recv().await {
                assert!(instant_is_approx(Instant::now(), expected));
            }
        });
    }

    #[test]
    fn test_loss() {
        drive_async_test(async {
            let packet_loss = 0.25;
            let (s, r) = SimulatedConditionsChannel::new(SimulatedConditions {
                delay: Duration::ZERO,
                packet_loss,
            });

            let count = 10000;

            tokio::spawn({
                async move {
                    for i in 0..count {
                        s.send(i).unwrap();
                    }
                }
            });

            let mut expected = 0;
            let mut num_lost = 0;
            while let Ok(received) = r.recv().await {
                num_lost += received - expected;
                expected = received + 1;
            }
            let lost_ratio = num_lost as f32 / count as f32;
            assert!((lost_ratio - packet_loss).abs() < 0.05);
        });
    }

    #[test]
    fn test_clone() {
        drive_async_test(async {
            let (s, r) = SimulatedConditionsChannel::<usize>::new(SimulatedConditions {
                delay: Duration::from_secs(60),
                packet_loss: 0.,
            });

            let s2 = s.clone();
            let s3 = s.clone();

            assert_eq!(r.try_recv(), Err(TryRecvError::Empty));
            drop(s2);

            assert_eq!(r.try_recv(), Err(TryRecvError::Empty));
            drop(s3);

            assert_eq!(r.try_recv(), Err(TryRecvError::Empty));
            drop(s);

            assert_eq!(r.try_recv(), Err(TryRecvError::Closed));
        });
    }

    #[test]
    fn test_close_sender() {
        drive_async_test(async {
            let (s, r) = SimulatedConditionsChannel::<usize>::new(SimulatedConditions {
                delay: Duration::from_secs(10),
                packet_loss: 0.,
            });

            assert_eq!(r.try_recv(), Err(TryRecvError::Empty));

            s.send(0).unwrap();
            assert_eq!(r.try_recv(), Err(TryRecvError::Empty));

            drop(s);
            assert_eq!(r.try_recv(), Err(TryRecvError::Empty));

            sleep_secs(20).await;
            assert_eq!(r.try_recv(), Ok(0));
            assert_eq!(r.try_recv(), Err(TryRecvError::Closed));

            drop(r);
        });
    }

    #[test]
    fn test_multiple_recv() {
        drive_async_test(async {
            let (s, r) = SimulatedConditionsChannel::<usize>::new(SimulatedConditions {
                delay: Duration::from_millis(100),
                packet_loss: 0.,
            });

            let r2 = r.clone();

            let t1 = async move {
                let mut count = 0;
                while r.recv().await.is_ok() {
                    count += 1;
                }
                count
            };

            let t2 = async move {
                let mut count = 0;
                while r2.recv().await.is_ok() {
                    count += 1;
                }
                count
            };

            tokio::spawn(async move {
                for i in 0..10 {
                    s.send(i).unwrap();
                    sleep_secs(1).await;
                }
                drop(s);
            });

            let (t1, t2) = tokio::join!(
                tokio::time::timeout(Duration::from_secs(60), t1),
                tokio::time::timeout(Duration::from_secs(60), t2)
            );

            assert_eq!(t1, Ok(5));
            assert_eq!(t2, Ok(5));
        });
    }
}
