use std::{fmt::Display, mem};

pub mod varint;
use varint::{VarIntEncoding, VarintDecodeError};

#[derive(Debug)]
pub enum Error {
    UnexpectedEnd,
    InvalidData,
    Other,
}

impl std::error::Error for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::UnexpectedEnd => write!(f, "Unexpected end of data"),
            Error::InvalidData => write!(f, "Invalid data"),
            Error::Other => write!(f, "Other"),
        }
    }
}

impl From<VarintDecodeError> for Error {
    fn from(_: VarintDecodeError) -> Self {
        Error::UnexpectedEnd
    }
}

pub type Result<T> = std::result::Result<T, Error>;

pub trait Remaining {
    fn remaining_bytes(&self) -> usize;

    #[inline]
    fn ensure_remaining(&self, size: usize) -> Result<()> {
        if self.remaining_bytes() < size {
            return Err(Error::UnexpectedEnd);
        }
        Ok(())
    }
}

pub trait RemainingMut: Remaining {
    fn remaining_slice_mut(&mut self) -> &mut [u8];
}

pub trait Writer: RemainingMut + Sized {
    fn unchecked_write(&mut self, data: &[u8]);

    fn unchecked_skip(&mut self, len: usize);

    #[inline]
    fn skip(&mut self, len: usize) -> Result<()> {
        self.ensure_remaining(len)?;
        self.unchecked_skip(len);
        Ok(())
    }

    #[inline]
    fn write(&mut self, data: &[u8]) -> Result<()> {
        self.ensure_remaining(data.len())?;
        self.unchecked_write(data);
        Ok(())
    }

    #[inline]
    fn write_u8(&mut self, value: u8) -> Result<()> {
        self.write(&[value])
    }

    #[inline]
    fn write_le<T: LittleEndian>(&mut self, value: T) -> Result<()> {
        self.write(value.to_le_bytes().as_ref())
    }

    #[inline]
    fn write_varint<T: VarIntEncoding>(&mut self, value: T) -> Result<()> {
        self.ensure_remaining(value.size())?;
        let size = value.encode(self.remaining_slice_mut());
        self.unchecked_skip(size);
        Ok(())
    }

    #[inline]
    fn encode<T: Encode>(&mut self, value: &T) -> Result<()> {
        value.encode(self)
    }
}

pub trait Reader<'storage>: Remaining + Sized {
    fn remaining_slice(&self) -> &'storage [u8];

    fn unchecked_read_slice(&mut self, len: usize) -> &'storage [u8];

    fn unchecked_skip(&mut self, len: usize);

    #[inline]
    fn skip(&mut self, len: usize) -> Result<()> {
        self.ensure_remaining(len)?;
        self.unchecked_skip(len);
        Ok(())
    }

    #[inline]
    fn read_slice(&mut self, len: usize) -> Result<&'storage [u8]> {
        self.ensure_remaining(len)?;
        Ok(self.unchecked_read_slice(len))
    }

    #[inline]
    fn read_array<const N: usize>(&mut self) -> Result<[u8; N]> {
        let mut array = [0; N];
        self.read(&mut array)?;
        Ok(array)
    }

    #[inline]
    fn read(&mut self, buf: &mut [u8]) -> Result<()> {
        let slice = self.read_slice(buf.len())?;
        buf.copy_from_slice(slice);
        Ok(())
    }

    #[inline]
    fn read_u8(&mut self) -> Result<u8> {
        Ok(self.read_slice(1)?[0])
    }

    #[inline]
    fn read_le<T: LittleEndian>(&mut self) -> Result<T> {
        Ok(T::from_le_slice(self.read_slice(T::BYTES)?))
    }

    #[inline]
    fn read_varint<T: VarIntEncoding>(&mut self) -> Result<T> {
        let slice = self.remaining_slice();
        let (decoded_value, size) = T::decode(slice)?;
        self.unchecked_skip(size);
        Ok(decoded_value)
    }

    #[inline]
    fn decode<T: BorrowDecode<'storage>>(&mut self) -> Result<T> {
        T::decode(self)
    }
}

impl Remaining for &[u8] {
    #[inline]
    fn remaining_bytes(&self) -> usize {
        self.len()
    }
}

impl<'storage> Reader<'storage> for &'storage [u8] {
    #[inline]
    fn unchecked_read_slice(&mut self, len: usize) -> &'storage [u8] {
        let (result, new_self) = self.split_at(len);
        *self = new_self;
        result
    }

    #[inline]
    fn unchecked_skip(&mut self, len: usize) {
        *self = &self[len..];
    }

    #[inline]
    fn remaining_slice(&self) -> &'storage [u8] {
        self
    }
}

impl Remaining for &mut [u8] {
    #[inline]
    fn remaining_bytes(&self) -> usize {
        self.len()
    }
}

impl RemainingMut for &mut [u8] {
    #[inline]
    fn remaining_slice_mut(&mut self) -> &mut [u8] {
        self
    }
}

impl Writer for &mut [u8] {
    #[inline]
    fn unchecked_write(&mut self, data: &[u8]) {
        let (dst, rest) = mem::take(self).split_at_mut(data.len());
        dst.copy_from_slice(data);
        *self = rest;
    }

    #[inline]
    fn unchecked_skip(&mut self, len: usize) {
        *self = &mut mem::take(self)[len..];
    }
}

pub trait LittleEndian {
    const BYTES: usize;
    type BytesT: AsRef<[u8]>;

    fn from_le_slice(slice: &[u8]) -> Self;
    fn to_le_bytes(self) -> Self::BytesT;
}

macro_rules! impl_endian {
    ($int_t:ident) => {
        impl LittleEndian for $int_t {
            const BYTES: usize = $int_t::BITS as usize / 8;
            type BytesT = [u8; Self::BYTES];

            #[inline]
            fn to_le_bytes(self) -> Self::BytesT {
                $int_t::to_le_bytes(self)
            }

            #[inline]
            fn from_le_slice(slice: &[u8]) -> Self {
                let mut buf = [0u8; Self::BYTES];
                buf.copy_from_slice(slice);
                $int_t::from_le_bytes(buf)
            }
        }
    };
}

impl_endian!(u8);
impl_endian!(u16);
impl_endian!(i16);
impl_endian!(u32);
impl_endian!(i32);
impl_endian!(u64);
impl_endian!(i64);
impl_endian!(u128);
impl_endian!(i128);

pub trait Encode: Sized {
    fn encode<W: Writer>(&self, writer: &mut W) -> Result<()>;
}

pub trait Decode: Sized {
    fn decode<'a, R: Reader<'a>>(reader: &mut R) -> Result<Self>;
}

pub trait BorrowDecode<'storage>: Sized {
    fn decode<R: Reader<'storage>>(reader: &mut R) -> Result<Self>;
}

impl<'storage, T: Decode> BorrowDecode<'storage> for T {
    #[inline]
    fn decode<R: Reader<'storage>>(reader: &mut R) -> Result<Self> {
        Decode::decode(reader)
    }
}
