use std::{
    error::Error,
    fmt::Display,
    ops::{BitAnd, BitOrAssign, BitXor, Neg, Shl, Shr, ShrAssign},
};

trait SignedInteger:
    Copy
    + Shl<usize, Output = Self>
    + Shr<usize, Output = Self>
    + BitXor<Output = Self>
    + Neg<Output = Self>
{
    type Unsigned: UnsignedInteger;
    const NUM_BITS: usize;

    fn as_unsigned(self) -> Self::Unsigned;
}

trait UnsignedInteger:
    Copy
    + Shl<usize, Output = Self>
    + Shr<usize, Output = Self>
    + ShrAssign<usize>
    + BitXor<Output = Self>
    + BitAnd<Output = Self>
    + BitOrAssign
    + From<u8>
    + PartialEq
    + PartialOrd
{
    type Signed: SignedInteger;

    const NUM_BITS: usize;
    const NUM_BYTES: usize;
    const ZERO: Self;
    const ONE: Self;

    fn as_signed(self) -> Self::Signed;
    fn as_u8(self) -> u8;
    fn twos_complement(self) -> Self;
    fn leading_zeros(self) -> u32;
}

pub trait VarIntEncoding: Copy {
    fn size(self) -> usize;
    fn encode(self, dst: &mut [u8]) -> usize;
    fn decode(src: &[u8]) -> Result<(Self, usize), VarintDecodeError>;
}

macro_rules! impl_int {
    ($unsigned_t:ident, $signed_t:ident) => {
        impl SignedInteger for $signed_t {
            type Unsigned = $unsigned_t;

            const NUM_BITS: usize = $unsigned_t::BITS as usize;

            fn as_unsigned(self) -> Self::Unsigned {
                self as $unsigned_t
            }
        }

        impl UnsignedInteger for $unsigned_t {
            type Signed = $signed_t;

            const NUM_BITS: usize = $unsigned_t::BITS as usize;
            const NUM_BYTES: usize = Self::NUM_BITS / 8;
            const ZERO: $unsigned_t = 0;
            const ONE: $unsigned_t = 1;

            fn as_signed(self) -> Self::Signed {
                self as $signed_t
            }

            fn as_u8(self) -> u8 {
                self as u8
            }

            fn twos_complement(self) -> Self {
                (-self.as_signed()).as_unsigned()
            }

            fn leading_zeros(self) -> u32 {
                self.leading_zeros()
            }
        }

        impl VarIntEncoding for $signed_t {
            fn size(self) -> usize {
                required_encoded_space_unsigned(zigzag_encode(self))
            }

            fn encode(self, dst: &mut [u8]) -> usize {
                varint_encode_unsigned(zigzag_encode(self), dst)
            }

            fn decode(src: &[u8]) -> Result<(Self, usize), VarintDecodeError> {
                varint_decode_unsigned::<$unsigned_t>(src)
                    .map(|(zz, size)| (zigzag_decode(zz), size))
            }
        }

        impl VarIntEncoding for $unsigned_t {
            fn size(self) -> usize {
                required_encoded_space_unsigned(self)
            }

            fn encode(self, dst: &mut [u8]) -> usize {
                varint_encode_unsigned(self, dst)
            }

            fn decode(src: &[u8]) -> Result<(Self, usize), VarintDecodeError> {
                varint_decode_unsigned::<$unsigned_t>(src)
            }
        }
    };
}

impl_int!(u16, i16);
impl_int!(u32, i32);
impl_int!(u64, i64);
impl_int!(u128, i128);

#[inline]
fn zigzag_encode<T: SignedInteger>(value: T) -> T::Unsigned {
    ((value << 1) ^ (value >> (T::NUM_BITS - 1))).as_unsigned()
}

#[inline]
fn zigzag_decode<T: UnsignedInteger>(encoded_value: T) -> T::Signed {
    ((encoded_value >> 1) ^ (encoded_value & T::ONE).twos_complement()).as_signed()
}

/// Most-significant byte, == 0x80
pub const MSB: u8 = 0b1000_0000;

#[inline]
fn varint_decode_unsigned<T: UnsignedInteger>(src: &[u8]) -> Result<(T, usize), VarintDecodeError> {
    let mut result = T::ZERO;
    let mut shift = 0;

    let max_bytes = (T::NUM_BITS + 6) / 7;
    for (i, b) in src.iter().enumerate().take(max_bytes) {
        let msb_dropped = b & !MSB;
        result |= T::from(msb_dropped) << shift;
        shift += 7;

        if b & MSB == 0 {
            return Ok((result, i + 1));
        }
    }
    Err(VarintDecodeError)
}

#[inline]
fn required_encoded_space_unsigned<T: UnsignedInteger>(v: T) -> usize {
    if v == T::ZERO {
        return 1;
    }
    ((T::NUM_BITS - v.leading_zeros() as usize) + 6) / 7
}

#[derive(Debug)]
pub struct VarintDecodeError;

impl Error for VarintDecodeError {}
impl Display for VarintDecodeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Varint encode error")
    }
}

#[inline]
fn varint_encode_unsigned<T: UnsignedInteger>(value: T, dst: &mut [u8]) -> usize {
    let len = required_encoded_space_unsigned(value);
    let mut n = value;
    for v in dst.iter_mut().take(len - 1) {
        *v = MSB | n.as_u8();
        n >>= 7;
    }

    dst[len - 1] = n.as_u8();
    len
}

#[inline]
pub fn varint_size<T: VarIntEncoding>(v: T) -> usize {
    v.size()
}

#[inline]
pub fn varint_encode<T: VarIntEncoding>(v: T, dst: &mut [u8]) -> usize {
    v.encode(dst)
}

#[inline]
pub fn varint_decode<T: VarIntEncoding>(src: &[u8]) -> Result<(T, usize), VarintDecodeError> {
    T::decode(src)
}

#[cfg(test)]
mod tests {
    use std::fmt::Debug;

    use rand::{distributions::Standard, prelude::Distribution, Rng, SeedableRng};
    use rand_xorshift::XorShiftRng;

    use super::{varint_decode, varint_encode, zigzag_decode, zigzag_encode, VarIntEncoding};

    const SEED: u64 = 1337;

    #[test]
    fn test_zigzag() {
        let mut rng = XorShiftRng::seed_from_u64(SEED);
        for _ in 0..2000 {
            let initial: i64 = rng.gen();
            let encoded: u64 = zigzag_encode(initial);
            let decoded: i64 = zigzag_decode(encoded);
            assert_eq!(decoded, initial);
        }
    }

    fn test_varint_type<T: VarIntEncoding>(max_value: T)
    where
        T: Debug + PartialEq,
        Standard: Distribution<T>,
    {
        let buffer = &mut [0; 32];
        let initial: T = max_value;
        let encoded_size = varint_encode(initial, buffer);
        let (decoded, decoded_size) = varint_decode::<T>(&buffer[..encoded_size]).unwrap();

        assert_eq!(encoded_size, decoded_size);
        assert_eq!(decoded, initial);

        let mut rng = XorShiftRng::seed_from_u64(SEED);
        for _ in 0..2000 {
            let buffer = &mut [0; 32];

            let initial: T = rng.gen();
            let encoded_size = varint_encode(initial, buffer);
            let (decoded, decoded_size) = varint_decode::<T>(&buffer[..encoded_size]).unwrap();

            assert_eq!(encoded_size, decoded_size);
            assert_eq!(decoded, initial);
        }
    }

    #[test]
    fn test_varint() {
        test_varint_type::<u16>(u16::MAX);
        test_varint_type::<i16>(i16::MAX);
        test_varint_type::<u32>(u32::MAX);
        test_varint_type::<i32>(i32::MAX);
        test_varint_type::<u64>(u64::MAX);
        test_varint_type::<i64>(i64::MAX);
        test_varint_type::<u128>(u128::MAX);
        test_varint_type::<i128>(i128::MAX);
    }
}
