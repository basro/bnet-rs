use parking_lot::Mutex;
use std::{future::Future, sync::Arc};
use tokio::sync::Notify;

struct FuturesTrackerInner {
    count: Mutex<usize>,
    notify: Notify,
}

#[derive(Clone)]
pub struct FuturesTracker {
    inner: Arc<FuturesTrackerInner>,
}

impl FuturesTracker {
    pub fn new() -> Self {
        Self {
            inner: Arc::new(FuturesTrackerInner {
                count: Mutex::new(0),
                notify: Notify::new(),
            }),
        }
    }

    pub fn track<T: Future>(&self, future: T) -> TrackFuture<T> {
        TrackFuture {
            fut: future,
            _guard: Guard::new(self),
        }
    }

    pub fn count(&self) -> usize {
        *self.inner.count.lock()
    }

    pub async fn wait_until_empty(&self) {
        let notified = self.inner.notify.notified();
        if self.count() == 0 {
            return;
        }
        notified.await;
    }
}

impl Default for FuturesTracker {
    fn default() -> Self {
        Self::new()
    }
}

pub struct TrackFuture<T: Future> {
    fut: T,
    _guard: Guard,
}

impl<T> Future for TrackFuture<T>
where
    T: Future,
{
    type Output = T::Output;

    fn poll(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Self::Output> {
        // TrackFuture does not manually implement Drop and does not expose the field fut therefore
        // it is safe to pin project.
        let pin_fut = unsafe { std::pin::Pin::map_unchecked_mut(self, |this| &mut this.fut) };
        Future::poll(pin_fut, cx)
    }
}

struct Guard {
    inner: Arc<FuturesTrackerInner>,
}

impl Guard {
    fn new(futures_tracker: &FuturesTracker) -> Self {
        let inner = futures_tracker.inner.clone();
        *inner.count.lock() += 1;
        Self { inner }
    }
}

impl Drop for Guard {
    fn drop(&mut self) {
        let mut count = self.inner.count.lock();
        *count -= 1;
        if *count == 0 {
            drop(count);
            self.inner.notify.notify_waiters()
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use tokio::time::Instant;

    use super::*;

    #[tokio::test(start_paused = true)]
    async fn test_count() {
        let tracker = FuturesTracker::new();

        assert_eq!(tracker.count(), 0);

        tokio::spawn(tracker.track(async { tokio::time::sleep(Duration::from_secs(4)).await }));
        assert_eq!(tracker.count(), 1);

        tokio::time::sleep(Duration::from_secs(2)).await;
        assert_eq!(tracker.count(), 1);

        tokio::spawn(tracker.track(async { tokio::time::sleep(Duration::from_secs(4)).await }));
        assert_eq!(tracker.count(), 2);

        tokio::time::sleep(Duration::from_secs(4)).await;
        assert_eq!(tracker.count(), 1);

        tokio::time::sleep(Duration::from_secs(4)).await;
        assert_eq!(tracker.count(), 0);
    }

    #[tokio::test(start_paused = true)]
    async fn test_wait_until_empty() {
        fn measure_time(future: impl Future) -> impl Future<Output = Duration> {
            let start = Instant::now();
            async move {
                tokio::time::timeout(Duration::from_secs(60), future)
                    .await
                    .unwrap();
                start.elapsed()
            }
        }

        fn is_approx(a: Duration, b: Duration) -> bool {
            a.as_millis().abs_diff(b.as_millis()) < 50
        }

        let tracker = FuturesTracker::new();

        let empty1 = measure_time(tokio::spawn({
            let tracker = tracker.clone();
            async move { tracker.wait_until_empty().await }
        }));
        tokio::spawn(tracker.track(async { tokio::time::sleep(Duration::from_secs(1)).await }));
        tokio::spawn(tracker.track(async { tokio::time::sleep(Duration::from_secs(6)).await }));
        tokio::time::sleep(Duration::from_secs(1)).await;
        tokio::spawn(tracker.track(async { tokio::time::sleep(Duration::from_secs(6)).await }));
        tokio::time::sleep(Duration::from_secs(1)).await;

        let empty2 = measure_time(tokio::spawn({
            let tracker = tracker.clone();
            async move { tracker.wait_until_empty().await }
        }));

        tokio::spawn(tracker.track(async { tokio::time::sleep(Duration::from_secs(6)).await }));
        tokio::time::sleep(Duration::from_secs(1)).await;
        tokio::spawn(tracker.track(async { tokio::time::sleep(Duration::from_secs(6)).await }));
        tokio::time::sleep(Duration::from_secs(5)).await;

        let empty3 = measure_time(tokio::spawn({
            let tracker = tracker.clone();
            async move { tracker.wait_until_empty().await }
        }));

        let (t1, t2, t3) = tokio::join!(empty1, empty2, empty3);

        assert!(is_approx(t1, Duration::from_secs(9)));
        assert!(is_approx(t2, Duration::from_secs(7)));
        assert!(is_approx(t3, Duration::from_secs(1)));
    }
}
