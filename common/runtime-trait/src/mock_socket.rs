use parking_lot::Mutex;
use std::{collections::HashMap, future::Future, io, net::SocketAddr, sync::Arc};

use async_channel as mpmc;

use crate::Socket;

type SockMessage = (Box<[u8]>, SocketAddr);

struct MockNetworkInner {
    sockets: HashMap<SocketAddr, mpmc::Sender<SockMessage>>,
}

#[derive(Clone)]
pub struct MockNetwork {
    inner: Arc<Mutex<MockNetworkInner>>,
}

impl MockNetwork {
    fn send(&self, buf: &[u8], target: SocketAddr, from: SocketAddr) {
        let inner = self.inner.lock();
        if let Some(target_sender) = inner.sockets.get(&target) {
            let _ = target_sender.try_send((buf.into(), from));
        }
    }

    pub fn new() -> Self {
        Self {
            inner: Arc::new(Mutex::new(MockNetworkInner {
                sockets: HashMap::new(),
            })),
        }
    }

    pub fn make_sock(&self, local_addr: SocketAddr) -> io::Result<MockSocket> {
        let mut inner = self.inner.lock();

        let mut local_addr = local_addr;
        if local_addr.port() == 0 {
            for new_port in 0..u16::MAX {
                local_addr.set_port(new_port);
                if !inner.sockets.contains_key(&local_addr) {
                    break;
                }
            }
        }

        if let Some(sender) = inner.sockets.get(&local_addr) {
            if !sender.is_closed() {
                return Err(io::ErrorKind::AddrInUse.into());
            }
        }

        let (sock_s, sock_r) = mpmc::unbounded();

        inner.sockets.insert(local_addr, sock_s);

        let sock = MockSocket {
            local_addr,
            mock_network: self.clone(),
            recv_ch: sock_r,
        };

        Ok(sock)
    }
}

impl Default for MockNetwork {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone)]
pub struct MockSocket {
    local_addr: SocketAddr,
    mock_network: MockNetwork,
    recv_ch: mpmc::Receiver<SockMessage>,
}

impl MockSocket {
    pub async fn send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()> {
        self.mock_network.send(buf, target, self.local_addr);
        Ok(())
    }

    pub async fn recv_from(&self, buf: &mut [u8]) -> io::Result<(usize, SocketAddr)> {
        let (msg, from) = self.recv_ch.recv().await.unwrap();
        let size = usize::min(msg.len(), buf.len());
        buf[..size].copy_from_slice(&msg[..size]);
        Ok((size, from))
    }

    pub fn try_send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()> {
        self.mock_network.send(buf, target, self.local_addr);
        Ok(())
    }

    pub fn local_addr(&self) -> SocketAddr {
        self.local_addr
    }
}

impl Socket for MockSocket {
    type SendToFuture<'a> = impl Future<Output = io::Result<()>>;
    fn send_to<'a>(&'a self, buf: &'a [u8], target: SocketAddr) -> Self::SendToFuture<'a> {
        MockSocket::send_to(self, buf, target)
    }

    type RecvFromFuture<'a> = impl Future<Output = io::Result<(usize, SocketAddr)>>;
    fn recv_from<'a>(&'a self, buf: &'a mut [u8]) -> Self::RecvFromFuture<'a> {
        MockSocket::recv_from(self, buf)
    }

    fn try_send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()> {
        MockSocket::try_send_to(self, buf, target)
    }
}
