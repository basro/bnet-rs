use futures_tracker::FuturesTracker;
use std::{cell::RefCell, future::Future, time::Duration};

use crate::Runtime;

thread_local! {
    static TASKS_TRACKER: RefCell<Option<FuturesTracker>> = RefCell::new(None);
}

#[derive(Copy, Clone)]
pub struct TrackedTokioRuntime();

struct ContextTrackerGuard {
    previous: Option<FuturesTracker>,
}

impl ContextTrackerGuard {
    pub fn new() -> (FuturesTracker, Self) {
        let current = FuturesTracker::new();
        let previous = TASKS_TRACKER.with(|tracker| tracker.replace(Some(current.clone())));
        (current, Self { previous })
    }
}

impl Drop for ContextTrackerGuard {
    fn drop(&mut self) {
        TASKS_TRACKER.with(|tracker| tracker.replace(self.previous.take()));
    }
}

impl TrackedTokioRuntime {
    pub fn block_on_and_wait_for_tasks(
        future: impl Future,
        max_tasks_time: Duration,
    ) -> Result<(), tokio::time::error::Elapsed> {
        let (current, _tracker_guard) = ContextTrackerGuard::new();

        let rt = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .on_thread_start({
                let current = current.clone();
                move || {
                    TASKS_TRACKER.with(|tracker| tracker.replace(Some(current.clone())));
                }
            })
            .on_thread_stop(|| {
                TASKS_TRACKER.with(|tracker| tracker.replace(None));
            })
            .build()
            .unwrap();

        rt.block_on(async {
            future.await;
            tokio::time::timeout(max_tasks_time, current.wait_until_empty()).await
        })
    }

    pub fn spawn<T>(future: T)
    where
        T: Future + Send + 'static,
        T::Output: Send + 'static,
    {
        TASKS_TRACKER.with(|tracker| {
            if let Some(tracker) = &*tracker.borrow() {
                ::tokio::spawn(tracker.track(future));
            } else {
                ::tokio::spawn(future);
            }
        });
    }
}

impl Runtime for TrackedTokioRuntime {
    type Instant = ::tokio::time::Instant;

    type SleepFuture = ::tokio::time::Sleep;
    fn sleep(duration: Duration) -> Self::SleepFuture {
        ::tokio::time::sleep(duration)
    }

    fn spawn<T>(future: T)
    where
        T: Future + Send + 'static,
        T::Output: Send + 'static,
    {
        TrackedTokioRuntime::spawn(future);
    }
}
