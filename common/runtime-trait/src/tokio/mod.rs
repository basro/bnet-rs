use ::tokio::net::UdpSocket;

use super::*;

#[derive(Copy, Clone)]
pub struct TokioRuntime();

impl Instant for ::tokio::time::Instant {
    fn now() -> Self {
        ::tokio::time::Instant::now()
    }
}

impl Runtime for TokioRuntime {
    type Instant = ::tokio::time::Instant;

    type SleepFuture = ::tokio::time::Sleep;
    fn sleep(duration: Duration) -> Self::SleepFuture {
        ::tokio::time::sleep(duration)
    }

    fn spawn<T>(future: T)
    where
        T: Future + Send + 'static,
        T::Output: Send + 'static,
    {
        ::tokio::spawn(future);
    }
}

impl Socket for UdpSocket {
    type SendToFuture<'a> = impl Future<Output = io::Result<()>>;
    fn send_to<'a>(&'a self, buf: &'a [u8], target: SocketAddr) -> Self::SendToFuture<'a> {
        async move { self.send_to(buf, target).await.map(|_| ()) }
    }

    type RecvFromFuture<'a> = impl Future<Output = io::Result<(usize, SocketAddr)>>;
    fn recv_from<'a>(&'a self, buf: &'a mut [u8]) -> Self::RecvFromFuture<'a> {
        self.recv_from(buf)
    }

    fn try_send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()> {
        self.try_send_to(buf, target).map(|_| ())
    }
}

#[cfg(feature = "test-utils")]
pub mod tracked;
