#![feature(type_alias_impl_trait)]

#[cfg(feature = "tokio")]
pub mod tokio;

#[cfg(feature = "test-utils")]
pub mod mock_socket;

use std::{
    future::Future,
    hash::Hash,
    io,
    net::SocketAddr,
    ops::{Add, AddAssign, Sub, SubAssign},
    panic::{RefUnwindSafe, UnwindSafe},
    time::Duration,
};

pub trait Instant:
    Send
    + Sync
    + RefUnwindSafe
    + UnwindSafe
    + Unpin
    + Copy
    + Clone
    + PartialEq
    + Eq
    + PartialOrd
    + Ord
    + Hash
    + Add<Duration, Output = Self>
    + AddAssign<Duration>
    + Sub<Duration, Output = Self>
    + SubAssign<Duration>
    + Sub<Output = Duration>
{
    fn now() -> Self;

    fn elapsed(&self) -> Duration {
        Self::now() - *self
    }
}

pub trait Runtime: Sync + Send + Copy + 'static {
    type Instant: Instant;

    type SleepFuture: Future<Output = ()> + Send;
    fn sleep(duration: Duration) -> Self::SleepFuture;

    fn spawn<T>(future: T)
    where
        T: Future + Send + 'static,
        T::Output: Send + 'static;
}

pub trait RuntimeExt: Runtime {
    fn sleep_until(deadline: Self::Instant) -> Self::SleepFuture;
}

impl<T: Runtime> RuntimeExt for T {
    fn sleep_until(deadline: Self::Instant) -> Self::SleepFuture {
        let now = Self::Instant::now();
        Self::sleep(deadline - now)
    }
}

pub trait Socket: Send + Sync + 'static {
    type SendToFuture<'a>: Future<Output = io::Result<()>> + Send
    where
        Self: 'a;
    fn send_to<'a>(&'a self, buf: &'a [u8], target: SocketAddr) -> Self::SendToFuture<'a>;

    type RecvFromFuture<'a>: Future<Output = io::Result<(usize, SocketAddr)>> + Send
    where
        Self: 'a;
    fn recv_from<'a>(&'a self, buf: &'a mut [u8]) -> Self::RecvFromFuture<'a>;

    fn try_send_to(&self, buf: &[u8], target: SocketAddr) -> io::Result<()>;
}
