use std::collections::HashMap;

pub struct OrderedReceiveQueue {
    next_sequence: u64,
    messages: HashMap<u64, Box<[u8]>>,
}

impl OrderedReceiveQueue {
    pub fn new() -> Self {
        Self {
            next_sequence: 0,
            messages: HashMap::new(),
        }
    }

    pub fn insert(&mut self, sequence: u64, data: &[u8]) {
        if sequence < self.next_sequence || self.messages.contains_key(&sequence) {
            return;
        }
        println!("received sequence {sequence}");
        self.messages.insert(sequence, data.into());
    }

    pub fn has_next(&self) -> bool {
        self.messages.contains_key(&self.next_sequence)
    }

    pub fn pop_next(&mut self) -> Option<Box<[u8]>> {
        if let Some(data) = self.messages.remove(&self.next_sequence) {
            self.next_sequence += 1;
            Some(data)
        } else {
            None
        }
    }
}
