use std::{convert::TryInto, fmt::Debug};

use codec::{BorrowDecode, Decode, Encode};

use crate::ack_set::AckSet;

#[derive(Debug)]
pub struct AckFrame {
    pub ack_set: AckSet,
    pub ack_delay: u16,
}

impl Encode for AckFrame {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.encode(&self.ack_set)?;
        writer.write_varint(self.ack_delay)?;
        Ok(())
    }
}

impl Decode for AckFrame {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        let ack_set = reader.decode()?;
        let ack_delay = reader.read_varint()?;
        Ok(Self { ack_set, ack_delay })
    }
}

#[derive(Debug)]
pub struct UnreliableMsgFrame<'storage> {
    pub data: &'storage [u8],
}

impl<'storage> Encode for UnreliableMsgFrame<'storage> {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        let len: u32 = self
            .data
            .len()
            .try_into()
            .map_err(|_| codec::Error::InvalidData)?;
        writer.write_varint(len)?;
        writer.write(self.data)
    }
}

impl<'storage> BorrowDecode<'storage> for UnreliableMsgFrame<'storage> {
    fn decode<R: codec::Reader<'storage>>(reader: &mut R) -> codec::Result<Self> {
        Ok(Self {
            data: {
                let len: u32 = reader.read_varint()?;
                reader.read_slice(len as usize)?
            },
        })
    }
}

pub struct ReliableMsgFrame<'storage> {
    pub fragment: bool,
    pub ordered: bool,
    pub sequence_number: u64,
    pub data: &'storage [u8],
}

impl<'storage> Debug for ReliableMsgFrame<'storage> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ReliableMsgFrame")
            .field("fragment", &self.fragment)
            .field("ordered", &self.ordered)
            .field("sequence_number", &self.sequence_number)
            .field("data.len", &self.data.len())
            .finish()
    }
}

impl<'storage> Encode for ReliableMsgFrame<'storage> {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        let flags: u8 = u8::from(self.fragment) | u8::from(self.ordered) << 1;
        writer.write_u8(flags)?;
        writer.write_varint(self.sequence_number)?;

        let len: u32 = self
            .data
            .len()
            .try_into()
            .map_err(|_| codec::Error::InvalidData)?;
        writer.write_varint(len)?;
        writer.write(self.data)
    }
}

impl<'storage> BorrowDecode<'storage> for ReliableMsgFrame<'storage> {
    fn decode<R: codec::Reader<'storage>>(reader: &mut R) -> codec::Result<Self> {
        let flags: u8 = reader.read_u8()?;
        Ok(Self {
            fragment: flags & 1 != 0,
            ordered: flags & 2 != 0,
            sequence_number: reader.read_varint()?,
            data: {
                let len: u32 = reader.read_varint()?;
                reader.read_slice(len as usize)?
            },
        })
    }
}

#[derive(Debug)]
pub struct StreamFrame {}

#[derive(Debug)]
pub struct SwitchChannelFrame {
    pub delta: i64,
}

impl Encode for SwitchChannelFrame {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.write_varint(self.delta)
    }
}

impl Decode for SwitchChannelFrame {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        Ok(Self {
            delta: reader.read_varint()?,
        })
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum FrameType {
    Ack,
    UnreliableMsg,
    ReliableMsg,
    Stream,
    Channel,
}

impl TryFrom<u8> for FrameType {
    type Error = ();

    fn try_from(v: u8) -> Result<Self, Self::Error> {
        use FrameType::*;
        match v {
            0 => Ok(Ack),
            1 => Ok(UnreliableMsg),
            2 => Ok(ReliableMsg),
            3 => Ok(Stream),
            4 => Ok(Channel),
            _ => Err(()),
        }
    }
}

impl From<FrameType> for u8 {
    fn from(ft: FrameType) -> Self {
        use FrameType::*;
        match ft {
            Ack => 0,
            UnreliableMsg => 1,
            ReliableMsg => 2,
            Stream => 3,
            Channel => 4,
        }
    }
}

impl From<&FrameType> for u8 {
    fn from(ft: &FrameType) -> Self {
        From::<FrameType>::from(*ft)
    }
}

impl From<&Frame<'_>> for FrameType {
    fn from(f: &Frame) -> Self {
        use Frame::*;
        match f {
            Ack(_) => FrameType::Ack,
            UnreliableMsg(_) => FrameType::UnreliableMsg,
            ReliableMsg(_) => FrameType::ReliableMsg,
            Stream(_) => FrameType::Stream,
            Channel(_) => FrameType::Channel,
        }
    }
}

impl Encode for FrameType {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        writer.write_le::<u8>(self.into())
    }
}

impl Decode for FrameType {
    fn decode<'a, R: codec::Reader<'a>>(reader: &mut R) -> codec::Result<Self> {
        reader
            .read_u8()?
            .try_into()
            .map_err(|_| codec::Error::InvalidData)
    }
}

#[derive(Debug)]
pub enum Frame<'storage> {
    Ack(AckFrame),
    UnreliableMsg(UnreliableMsgFrame<'storage>),
    ReliableMsg(ReliableMsgFrame<'storage>),
    Stream(StreamFrame),
    Channel(SwitchChannelFrame),
}

impl<'storage> BorrowDecode<'storage> for Frame<'storage> {
    fn decode<R: codec::Reader<'storage>>(reader: &mut R) -> codec::Result<Self> {
        let ft: FrameType = reader.decode()?;
        use Frame::*;
        Ok(match ft {
            FrameType::Ack => Ack(reader.decode()?),
            FrameType::UnreliableMsg => UnreliableMsg(reader.decode()?),
            FrameType::ReliableMsg => ReliableMsg(reader.decode()?),
            FrameType::Stream => Stream(StreamFrame {}),
            FrameType::Channel => Channel(reader.decode()?),
        })
    }
}

impl<'storage> Encode for Frame<'storage> {
    fn encode<W: codec::Writer>(&self, writer: &mut W) -> codec::Result<()> {
        let ft = FrameType::from(self);
        writer.write_le::<u8>(ft.into())?;
        match self {
            Frame::Ack(data) => writer.encode(data)?,
            Frame::UnreliableMsg(data) => writer.encode(data)?,
            Frame::ReliableMsg(data) => writer.encode(data)?,
            Frame::Stream(_) => todo!(),
            Frame::Channel(data) => writer.encode(data)?,
        }
        Ok(())
    }
}
