use runtime_trait::tokio::tracked::TrackedTokioRuntime;
use std::{future::Future, time::Duration};

pub fn drive_async_test(future: impl Future) {
    TrackedTokioRuntime::block_on_and_wait_for_tasks(
        async {
            tokio::time::pause();
            future.await;
        },
        Duration::from_secs(30),
    )
    .expect("No tasks should leak")
}
