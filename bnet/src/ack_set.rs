use codec::{Decode, Encode};

type AckSetBits = u64;

#[derive(Debug, Copy, Clone)]
pub struct AckSet {
    largest: u64,
    bits: AckSetBits, // Least significant bit represents packet <largest - 1>
}

impl AckSet {
    pub fn new() -> Self {
        Self {
            largest: u64::MAX,
            bits: 0,
        }
    }

    pub fn largest(&self) -> Option<u64> {
        if !self.is_empty() {
            Some(self.largest)
        } else {
            None
        }
    }

    pub fn is_empty(&self) -> bool {
        self.largest == u64::MAX
    }

    pub fn has(&self, pn: u64) -> bool {
        if pn > self.largest || pn < self.start() {
            return false;
        }

        if pn == self.largest {
            return true;
        }

        let shift = (self.largest - pn) - 1;
        self.bits & (1 << shift) != 0
    }

    fn start(&self) -> u64 {
        self.largest.saturating_sub(AckSetBits::BITS.into())
    }

    pub fn add(&mut self, packet_number: u64) {
        if packet_number == u64::MAX {
            return;
        }

        if self.largest == u64::MAX {
            self.largest = packet_number;
            return;
        }

        if packet_number == self.largest || packet_number < self.start() {
            return;
        }

        if packet_number > self.largest {
            let mut shift = packet_number - self.largest;
            if shift < AckSetBits::BITS.into() {
                self.bits <<= shift;
            } else {
                self.bits = 0
            }

            // What used to be the largest packet was not included in self.bits,
            // We now set that bit.
            shift -= 1;
            if shift < AckSetBits::BITS.into() {
                self.bits |= 1 << shift;
            }

            self.largest = packet_number;
        } else {
            self.bits |= 1 << ((self.largest - packet_number) - 1);
        }
    }

    pub fn iter(&self) -> AckSetIter {
        AckSetIter {
            largest: self.largest,
            bits: self.bits,
        }
    }
}

impl Default for AckSet {
    fn default() -> Self {
        Self::new()
    }
}

pub struct AckSetIter {
    largest: u64,
    bits: AckSetBits,
}

impl Iterator for AckSetIter {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        if self.largest == u64::MAX {
            return None;
        }

        let result = self.largest;

        if self.bits == 0 {
            self.largest = u64::MAX;
        } else {
            let shift = self.bits.trailing_zeros() + 1;
            self.bits = self.bits.checked_shr(shift).unwrap_or(0);
            self.largest = self.largest.checked_sub(shift.into()).unwrap_or(u64::MAX);
        }
        Some(result)
    }
}

impl Decode for AckSet {
    fn decode<'storage, R: codec::Reader<'storage> + ?Sized>(
        reader: &mut R,
    ) -> codec::Result<Self> {
        Ok(Self {
            largest: reader.read_varint()?,
            bits: reader.read_le()?,
        })
    }
}

impl Encode for AckSet {
    fn encode<W: codec::Writer + ?Sized>(&self, writer: &mut W) -> codec::Result<()> {
        writer.write_varint(self.largest)?;
        writer.write_le(self.bits)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::ack_set::AckSetBits;

    use super::AckSet;

    #[allow(clippy::bool_assert_comparison)]
    #[test]
    fn test_ack_set() {
        let bits_size: u64 = AckSetBits::BITS.into();

        let mut ack_set = AckSet::new();
        assert_eq!(ack_set.has(0), false);
        ack_set.add(0);
        assert_eq!(ack_set.has(0), true);
        assert_eq!(ack_set.has(10), false);
        ack_set.add(10);
        ack_set.add(bits_size);
        assert_eq!(ack_set.has(0), true);
        assert_eq!(ack_set.has(10), true);
        assert_eq!(ack_set.has(bits_size), true);
        assert_eq!(ack_set.has(bits_size + 1), false);
        ack_set.add(bits_size + 1);
        ack_set.add(0); // Is ignored
        assert_eq!(ack_set.has(0), false);
        assert_eq!(ack_set.has(1), false);
        assert_eq!(ack_set.has(10), true);
        assert_eq!(ack_set.has(bits_size - 1), false);
        assert_eq!(ack_set.has(bits_size), true);
        assert_eq!(ack_set.has(bits_size + 1), true);
        assert_eq!(ack_set.has(bits_size + 2), false);
        ack_set.add(bits_size + 10);
        assert_eq!(ack_set.has(0), false);
        assert_eq!(ack_set.has(10), true);
        assert_eq!(ack_set.has(bits_size), true);
        assert_eq!(ack_set.has(bits_size + 1), true);
        assert_eq!(ack_set.has(bits_size + 9), false);
        assert_eq!(ack_set.has(bits_size + 10), true);
        ack_set.add(5000);
        assert_eq!(ack_set.has(0), false);
        assert_eq!(ack_set.has(10), false);
        assert_eq!(ack_set.has(bits_size), false);
        assert_eq!(ack_set.has(bits_size + 1), false);
        assert_eq!(ack_set.has(bits_size + 10), false);
        assert_eq!(ack_set.has(5000), true);
        ack_set.add(5000 - bits_size);
        assert_eq!(ack_set.has(5000 - bits_size), true);
        assert_eq!(ack_set.has(5000), true);
    }

    #[test]
    fn test_iter() {
        let bits_size: u64 = AckSetBits::BITS.into();

        let mut ack_set = AckSet::new();

        assert_eq!(ack_set.iter().take(10).collect::<Vec<u64>>(), vec![]);

        ack_set.add(4);
        ack_set.add(15);
        ack_set.add(22);

        assert_eq!(
            ack_set.iter().take(10).collect::<Vec<u64>>(),
            vec![22, 15, 4]
        );

        let mut ack_set = AckSet::new();
        ack_set.add(bits_size);
        ack_set.add(0);
        assert_eq!(
            ack_set.iter().take(10).collect::<Vec<u64>>(),
            vec![bits_size, 0]
        );
        ack_set.add(bits_size + 1);

        assert_eq!(
            ack_set.iter().take(10).collect::<Vec<u64>>(),
            vec![bits_size + 1, bits_size]
        );

        let ack_set = AckSet {
            largest: 1,
            bits: 0b1000001,
        };

        assert_eq!(ack_set.iter().take(10).collect::<Vec<u64>>(), vec![1, 0]);
    }
}
