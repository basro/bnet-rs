use std::collections::VecDeque;

#[derive(Debug)]
pub struct ReceivedSequenceSet {
    flags: VecDeque<bool>,
    start_sequence: u64,
}

impl ReceivedSequenceSet {
    pub fn new() -> Self {
        Self {
            flags: VecDeque::new(),
            start_sequence: 0,
        }
    }

    pub fn get(&self, sequence: u64) -> bool {
        if sequence < self.start_sequence {
            return true;
        }

        let diff = (sequence - self.start_sequence) as usize;
        if diff >= self.flags.len() {
            return false;
        }
        self.flags[diff]
    }

    pub fn set(&mut self, sequence: u64) -> bool {
        if sequence < self.start_sequence {
            return false;
        }

        let diff = (sequence - self.start_sequence) as usize;

        if diff >= self.flags.len() {
            self.flags.resize(diff + 1, false);
        } else if self.flags[diff] {
            return false;
        }

        self.flags[diff] = true;

        self.trim();

        true
    }

    fn trim(&mut self) {
        let count = self.flags.iter().take_while(|v| **v).count();
        self.start_sequence += count as u64;
        self.flags.drain(..count);
    }

    pub fn size(&self) -> usize {
        self.flags.len()
    }
}

#[allow(clippy::bool_assert_comparison)]
#[test]
fn test_receved_sequence_set() {
    let mut rss = ReceivedSequenceSet::new();
    assert_eq!(rss.get(0), false);
    assert_eq!(rss.get(1), false);
    assert_eq!(rss.size(), 0);
    rss.set(1);
    assert_eq!(rss.size(), 2);
    assert_eq!(rss.get(0), false);
    assert_eq!(rss.get(1), true);
    assert_eq!(rss.get(2), false);
    rss.set(0);
    assert_eq!(rss.size(), 0);
    assert_eq!(rss.get(0), true);
    assert_eq!(rss.get(1), true);
    assert_eq!(rss.get(15), false);
    rss.set(15);
    assert_eq!(rss.size(), 14);
    assert_eq!(rss.get(0), true);
    assert_eq!(rss.get(1), true);
    assert_eq!(rss.get(14), false);
    assert_eq!(rss.get(15), true);
    assert_eq!(rss.get(16), false);
    for i in 3..15 {
        rss.set(i);
    }
    assert_eq!(rss.size(), 14);
    rss.set(2);
    assert_eq!(rss.size(), 0);
}
