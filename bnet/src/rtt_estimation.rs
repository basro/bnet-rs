#[derive(Debug)]
pub struct RoundTripTimeEstimation {
    latest: f32,
    smoothed: f32,
    min: f32,
    variance: f32,
}

fn lerp(factor: f32, src: f32, dst: f32) -> f32 {
    src * (1. - factor) + dst * factor
}

impl RoundTripTimeEstimation {
    pub(crate) fn new(latest_rtt: f32) -> Self {
        Self {
            latest: latest_rtt,
            smoothed: latest_rtt,
            min: latest_rtt,
            variance: latest_rtt / 2.,
        }
    }

    pub(crate) fn update(&mut self, ack_delay: f32, latest_rtt: f32) {
        self.latest = latest_rtt;
        self.min = f32::min(self.min, latest_rtt);

        let adjusted_rtt = if self.min + ack_delay < latest_rtt {
            latest_rtt - ack_delay
        } else {
            latest_rtt
        };

        self.smoothed = lerp(0.125, self.smoothed, adjusted_rtt);
        let variance_sample = f32::abs(self.smoothed - adjusted_rtt);
        self.variance = lerp(0.25, self.variance, variance_sample);
    }

    pub fn latest(&self) -> f32 {
        self.latest
    }

    pub fn smoothed(&self) -> f32 {
        self.smoothed
    }

    pub fn min(&self) -> f32 {
        self.min
    }

    pub fn variance(&self) -> f32 {
        self.variance
    }
}
