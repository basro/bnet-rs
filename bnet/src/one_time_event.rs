use futures::{
    channel::oneshot,
    future::{pending, Either, Shared},
    FutureExt, TryFutureExt,
};
use std::future::Future;

#[derive(Debug)]
struct UndispatchedState {
    sender: oneshot::Sender<()>,
    receiver: Shared<oneshot::Receiver<()>>,
}

#[derive(Debug)]
pub struct OneTimeEvent(Option<UndispatchedState>);

impl OneTimeEvent {
    pub fn new() -> Self {
        let (sender, receiver) = oneshot::channel();
        Self(Some(UndispatchedState {
            sender,
            receiver: receiver.shared(),
        }))
    }

    pub fn dispatch(&mut self) {
        if let Some(undispatched_state) = self.0.take() {
            let _ = undispatched_state.sender.send(());
        }
    }

    pub fn dispatched(&self) -> impl Future<Output = ()> {
        if let Some(undispatched_state) = &self.0 {
            Either::Left({
                undispatched_state
                    .receiver
                    .clone()
                    .into_future()
                    .then(|r| async move {
                        if r.is_err() {
                            pending().await
                        }
                    })
            })
        } else {
            Either::Right(async {})
        }
    }

    pub fn is_dispatched(&self) -> bool {
        self.0.is_none()
    }
}

#[tokio::test]
async fn test() {
    let mut ote = OneTimeEvent::new();

    let d1 = ote.dispatched();
    let d2 = ote.dispatched();

    assert_eq!(ote.dispatched().now_or_never(), None);

    ote.dispatch();

    ote.dispatched().await;

    d2.await;
    drop(ote);
    d1.await;
}
