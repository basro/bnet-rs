#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Reliability {
    Reliable,
    Unreliable,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Ordering {
    Ordered,
    Unordered,
}

pub struct Message {
    pub reliability: Reliability,
    pub ordering: Ordering,
    pub sequence_number: u64,
    pub data: Box<[u8]>,
}
