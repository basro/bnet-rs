use std::{
    collections::VecDeque,
    convert::TryInto,
    future::{pending, Future},
    sync::Arc,
    time::Duration,
};

use codec::{Reader, Remaining, Writer};
use layer0::connection::ReceivedPayload;
use linked_hash_map::LinkedHashMap;
use parking_lot::Mutex;
use runtime_trait::Socket;
use tokio::{
    select,
    sync::{mpsc, watch, Notify},
    time::Instant,
};

use crate::{
    ack_set::AckSet,
    frame::{AckFrame, Frame, ReliableMsgFrame, UnreliableMsgFrame},
    message::{Message, Ordering, Reliability},
    one_time_event::OneTimeEvent,
    ordered_receive_queue::OrderedReceiveQueue,
    received_sequence_set::ReceivedSequenceSet,
    rtt_estimation::RoundTripTimeEstimation,
};

type L0Connection<T> = layer0::connection::ConnectionHandle<T>;

struct SentPacket {
    packet_number: u64,
    sent_time: Instant,
    messages: Vec<Message>, // Todo: add list of messages
    ack_eliciting: bool,
}

struct ConnectionInnerMut<SocketT: Socket> {
    conn: L0Connection<SocketT>,
    close_event: OneTimeEvent,
    ack_set: AckSet,
    largest_packet_received_time: Instant,
    force_send: Option<bool>,
    outgoing_messages: VecDeque<Message>,
    sent_packets: LinkedHashMap<u64, SentPacket>,
    largest_ack_received: Option<u64>,
    largest_ack_sent: Option<u64>,
    next_ro_sequence_number: u64,
    next_ru_sequence_number: u64,
    received_ru_sequences: ReceivedSequenceSet,
    ordered_receive_queue: OrderedReceiveQueue,
    recv_channel: mpsc::UnboundedSender<Box<[u8]>>,
    last_send_time: Instant,
    last_sent_ack_eliciting_time: Instant,
    last_pto_triggered_time: Instant,
    rtt_estimation: Option<RoundTripTimeEstimation>,
    name: Option<String>,

    probe_timeout: watch::Sender<Option<Instant>>,
    pto_count: u32,
}

const KEEP_ALIVE_DELAY: Duration = Duration::from_secs(5);
const MAX_ACK_DELAY: u16 = 9999;
const LOSS_TIME_THRESHOLD: f32 = 9. / 8.;
const LOSS_GRANULARITY: Duration = Duration::from_millis(2);

impl<SocketT: Socket> ConnectionInnerMut<SocketT> {
    fn close(&mut self) {
        self.close_event.dispatch();
    }

    fn is_closed(&mut self) -> bool {
        self.close_event.is_dispatched()
    }

    fn closed(&mut self) -> impl Future<Output = ()> {
        self.close_event.dispatched()
    }

    fn send(&mut self, data: &[u8], reliability: Reliability, ordering: Ordering) {
        fn increment_and_return_prev(v: &mut u64) -> u64 {
            let prev = *v;
            *v += 1;
            prev
        }

        self.outgoing_messages.push_back(Message {
            reliability,
            ordering,
            sequence_number: if reliability == Reliability::Reliable {
                match ordering {
                    Ordering::Ordered => {
                        increment_and_return_prev(&mut self.next_ro_sequence_number)
                    }
                    Ordering::Unordered => {
                        increment_and_return_prev(&mut self.next_ru_sequence_number)
                    }
                }
            } else {
                0
            },
            data: data.into(),
        });
    }

    fn log(&self, text: &str) {
        if let Some(name) = &self.name {
            println!("[{name}] {text}");
        }
    }

    fn loss_delay(&self) -> Duration {
        if let Some(rtt_estimation) = &self.rtt_estimation {
            Duration::from_secs_f32(f32::max(
                LOSS_TIME_THRESHOLD * f32::max(rtt_estimation.smoothed(), rtt_estimation.latest()),
                LOSS_GRANULARITY.as_secs_f32(),
            ))
        } else {
            Duration::from_millis(500)
        }
    }

    fn detect_lost_packets(&mut self) {
        let largest_ack_received = match self.largest_ack_received {
            Some(lar) => lar,
            None => return,
        };

        let max_packet_reorder = 4;
        let min_packet_number = largest_ack_received.saturating_sub(max_packet_reorder);
        let loss_delay = self.loss_delay();

        for sent_packet_entry in self.sent_packets.entries() {
            let entry_pn = *sent_packet_entry.key();

            if entry_pn >= min_packet_number
                && sent_packet_entry.get().sent_time.elapsed() < loss_delay
            {
                break;
            }

            let lost_packet = sent_packet_entry.remove();
            self.outgoing_messages.extend(lost_packet.messages);

            if let Some(name) = &self.name {
                if entry_pn < min_packet_number {
                    println!(
                        "[{name}] packet lost due to reorder: {}",
                        lost_packet.packet_number
                    );
                } else {
                    println!(
                        "[{name}] packet lost due to reaorder timeout: {}",
                        lost_packet.packet_number
                    );
                }
            }
        }
    }

    // fn detect_packet_timeout(&mut self) -> Option<Instant> {
    //     let loss_delay = self.loss_delay();

    //     for sent_packet_entry in self.sent_packets.entries() {
    //         let deadline = sent_packet_entry.get().sent_time + loss_delay;
    //         if deadline.elapsed().is_zero() {
    //             return Some(deadline);
    //         }

    //         let lost_packet = sent_packet_entry.remove();
    //         self.outgoing_messages.extend(lost_packet.messages);

    //         if let Some(name) = &self.name {
    //             println!(
    //                 "[{name}] packet lost due to timeout: {}",
    //                 lost_packet.packet_number
    //             );
    //         }
    //     }
    //     None
    // }

    fn handle_received_payload(&mut self, payload: &ReceivedPayload, notify_send: &Notify) {
        self.log(&format!(
            "received: pn({}), code({}), {}",
            payload.packet_number,
            payload.code,
            payload.data().len()
        ));

        // If the newly received packet is larger than the largest received
        if self
            .ack_set
            .largest()
            .filter(|pn| *pn >= payload.packet_number)
            .is_none()
        {
            self.largest_packet_received_time = Instant::now();
        }

        self.ack_set.add(payload.packet_number);

        let flags = payload.code & 1;
        let ack_eliciting = flags & 1 != 0;
        let code = payload.code >> 1;

        match code {
            1 => {
                self.close();
                return;
            }
            0 => {}
            _ => return,
        }

        let mut reader = payload.data();
        while reader.remaining_bytes() != 0 {
            use Frame::*;
            let frame = reader.decode::<Frame>();
            //self.log(&format!("{frame:?}"));
            match frame {
                Ok(Ack(ack)) => self.handle_ack_frame(&ack),
                Ok(ReliableMsg(msg)) => {
                    if msg.ordered {
                        self.ordered_receive_queue
                            .insert(msg.sequence_number, msg.data);
                        while let Some(msg) = self.ordered_receive_queue.pop_next() {
                            let _ = self.recv_channel.send(msg);
                            // TODO: Handle recv_channel send error.
                        }
                    } else {
                        // Unordered
                        if self.received_ru_sequences.set(msg.sequence_number) {
                            self.log("received ru msg")
                        }
                    }
                }
                Ok(_) => {}
                Err(_) => {
                    todo!();
                }
            }
        }

        // Not all packets are ack eliciting. However, if more than 8 packets have been
        // received since the last time an ack was sent then we will force an ack.
        if ack_eliciting || self.largest_ack_sent.unwrap_or(0) + 8 < payload.packet_number {
            self.set_force_send(false, notify_send);
        }

        if self.should_send() {
            notify_send.notify_waiters()
        }
    }

    fn handle_ack_frame(&mut self, ack: &AckFrame) {
        let largest_ack = match ack.ack_set.largest() {
            Some(v) => v,
            None => return,
        };

        if self.conn.set_largest_ack_received(largest_ack).is_err() {
            // TODO: For now we ignore the ack, but maybe we should
            // close the connection with an error?
            return;
        }

        self.log(&format!("Received ack {largest_ack}"));

        if self
            .largest_ack_received
            .filter(|&lar| lar >= largest_ack)
            .is_none()
        {
            self.largest_ack_received = Some(largest_ack);
        }

        if let Some(sent_packet) = self
            .sent_packets
            .get(&largest_ack)
            .filter(|sp| sp.ack_eliciting)
        {
            if ack.ack_delay < MAX_ACK_DELAY {
                let ack_delay = f32::from(ack.ack_delay) / 1000.;
                let latest_rtt = sent_packet.sent_time.elapsed().as_secs_f32();
                self.log(&format!(
                    "({largest_ack}) ack_delay: {ack_delay} rtt: {latest_rtt}"
                ));

                if let Some(rtt_estimation) = &mut self.rtt_estimation {
                    rtt_estimation.update(ack_delay, latest_rtt);
                    if let Some(name) = &self.name {
                        println!("[{name}] {rtt_estimation:?}");
                    }
                } else {
                    self.rtt_estimation = Some(RoundTripTimeEstimation::new(latest_rtt));
                    if let Some(name) = &self.name {
                        println!("[{name}]{:?}", self.rtt_estimation.as_ref().unwrap());
                    }
                }
            }
        }

        for acked_packet in ack.ack_set.iter() {
            if let Some(_acked_packet_data) = self.sent_packets.remove(&acked_packet) {
                //println!("packet {} got acked!", acked_packet);
            }
        }

        self.detect_lost_packets();
        self.pto_count = 0;
        self.update_pto();
    }

    fn should_send(&self) -> bool {
        self.force_send.is_some() || !self.outgoing_messages.is_empty()
    }

    fn set_force_send(&mut self, ack_eliciting: bool, notify_send: &Notify) {
        self.force_send = Some(self.force_send.unwrap_or(false) || ack_eliciting);
        notify_send.notify_waiters();
    }

    fn build_payload(&mut self, buf: &mut [u8], packet_number: u64) -> (u8, usize) {
        let mut writer = &mut buf[..];

        writer
            .encode(&Frame::Ack(AckFrame {
                ack_set: self.ack_set,
                ack_delay: self
                    .largest_packet_received_time
                    .elapsed()
                    .as_millis()
                    .min(MAX_ACK_DELAY.into())
                    .try_into()
                    .unwrap(),
            }))
            .expect("There should be enough space to encode ackset");
        self.largest_ack_sent = self.ack_set.largest();

        let mut sent_messages = Vec::new();
        while let Some(msg) = self
            .outgoing_messages
            .front()
            .filter(|msg| msg.data.len() + 16 < writer.remaining_bytes())
        {
            use Reliability::*;
            let frame = match msg.reliability {
                Unreliable => Frame::UnreliableMsg(UnreliableMsgFrame { data: &msg.data }),
                Reliable => Frame::ReliableMsg(ReliableMsgFrame {
                    data: &msg.data,
                    fragment: false,
                    ordered: msg.ordering == Ordering::Ordered,
                    sequence_number: msg.sequence_number,
                }),
            };
            writer
                .encode(&frame) // Encoding should always succeed
                .unwrap();

            let msg = self
                .outgoing_messages
                .pop_front() // while loop condition ensures pop will succeed
                .unwrap();
            if msg.reliability == Reliable {
                sent_messages.push(msg);
            }
        }

        let ack_eliciting = self.force_send.take().unwrap_or(false)
            || !sent_messages.is_empty()
            || self.rtt_estimation.is_none();

        if ack_eliciting {
            self.last_sent_ack_eliciting_time = Instant::now();
        }

        debug_assert!(self
            .sent_packets
            .back()
            .filter(|(&back_pn, _)| back_pn >= packet_number)
            .is_none());

        self.sent_packets.insert(
            packet_number,
            SentPacket {
                packet_number,
                sent_time: Instant::now(),
                messages: sent_messages,
                ack_eliciting,
            },
        );

        self.log(&format!(
            "outgoing packet: {packet_number}, ack_eliciting: {ack_eliciting}"
        ));

        self.last_send_time = Instant::now();

        self.update_pto();

        let mut flags = 0;
        if ack_eliciting {
            flags |= 1;
        }

        let remaining_bytes = writer.remaining_bytes();
        (flags, buf.len() - remaining_bytes)
    }

    fn probe_delay(&self) -> Duration {
        if let Some(rtt_estimation) = &self.rtt_estimation {
            Duration::from_secs_f32(
                LOSS_TIME_THRESHOLD * rtt_estimation.smoothed()
                    + f32::max(
                        4. * rtt_estimation.variance(),
                        LOSS_GRANULARITY.as_secs_f32(),
                    ),
            )
            .mul_f32(1.5f32.powf(self.pto_count as f32))
        } else {
            Duration::from_millis(500)
        }
    }

    fn update_pto(&mut self) {
        if self
            .sent_packets
            .iter()
            .all(|(_, packet)| !packet.ack_eliciting)
        {
            // Replace the probe timeout with None and end only
            // if previous value was not None
            self.probe_timeout.send_if_modified(|v| v.take().is_some());
            return;
        }

        let delay = self.probe_delay();
        let timeout = Instant::max(
            self.last_pto_triggered_time,
            self.last_sent_ack_eliciting_time,
        ) + delay;

        self.probe_timeout
            .send_if_modified(|v| v.replace(timeout) != Some(timeout));
    }
}

pub struct Connection<SocketT: Socket> {
    inner_mut: Mutex<ConnectionInnerMut<SocketT>>,
    notify_send: Notify,
}

type NewConnection<SocketT> = (Arc<Connection<SocketT>>, mpsc::UnboundedReceiver<Box<[u8]>>);

impl<SocketT: Socket> Connection<SocketT> {
    pub fn new(conn: L0Connection<SocketT>) -> NewConnection<SocketT> {
        let (recv_s, recv_r) = mpsc::unbounded_channel();
        let (probe_timeout_s, probe_timeout_r) = watch::channel(None);

        let this = Arc::new(Self {
            inner_mut: Mutex::new(ConnectionInnerMut {
                conn,
                close_event: OneTimeEvent::new(),
                ack_set: AckSet::new(),
                largest_packet_received_time: Instant::now(),
                outgoing_messages: VecDeque::new(),
                force_send: None,
                sent_packets: LinkedHashMap::new(),
                largest_ack_received: None,
                largest_ack_sent: None,
                next_ro_sequence_number: 0,
                next_ru_sequence_number: 0,
                received_ru_sequences: ReceivedSequenceSet::new(),
                ordered_receive_queue: OrderedReceiveQueue::new(),
                recv_channel: recv_s,
                last_send_time: Instant::now(),
                rtt_estimation: None,
                name: None,
                last_sent_ack_eliciting_time: Instant::now(),
                last_pto_triggered_time: Instant::now(),
                probe_timeout: probe_timeout_s,
                pto_count: 0,
            }),
            notify_send: Notify::new(),
        });

        tokio::spawn(driver(this.clone(), probe_timeout_r));
        (this, recv_r)
    }

    pub fn close(&self) {
        self.inner_mut.lock().close();
    }

    pub fn is_closed(&self) -> bool {
        self.inner_mut.lock().is_closed()
    }

    pub fn closed(&self) -> impl Future<Output = ()> {
        self.inner_mut.lock().closed()
    }

    pub fn send(&self, data: &[u8], reliability: Reliability, ordering: Ordering) {
        self.inner_mut.lock().send(data, reliability, ordering);
        self.notify_send.notify_waiters();
    }

    fn handle_received_payload(&self, payload: &ReceivedPayload) {
        self.inner_mut
            .lock()
            .handle_received_payload(payload, &self.notify_send)
    }

    pub fn set_name(&self, name: &str) {
        self.inner_mut.lock().name = Some(name.into());
    }

    fn trigger_pto(&self) {
        let mut inner = self.inner_mut.lock();
        inner.log(&format!("pto triggered {}", inner.pto_count));
        inner.set_force_send(true, &self.notify_send);
        inner.pto_count += 1;
        inner.last_pto_triggered_time = Instant::now();
        inner.update_pto();
    }
}

const CONNECTION_TIMEOUT: Duration = Duration::from_secs(15);

async fn driver<SocketT: Socket>(
    conn: Arc<Connection<SocketT>>,
    probe_timeout: watch::Receiver<Option<Instant>>,
) {
    let last_recv_time = Mutex::new(Instant::now());

    let l0_conn = conn.inner_mut.lock().conn.clone();

    let recv_loop = async {
        while let Some(data) = l0_conn.recv().await {
            *last_recv_time.lock() = Instant::now();
            conn.handle_received_payload(&data);
        }
    };

    let send_loop = async {
        let mut buf = [0; layer0::MAX_PAYLOAD_LENGTH];
        loop {
            let send_notified = conn.notify_send.notified();

            loop {
                let (reserved_pn, flags, size) = {
                    let mut conn_lock = conn.inner_mut.lock();
                    if !conn_lock.should_send() {
                        break;
                    }

                    let reserved_pn = l0_conn.reserve_packet_number();
                    let (flags, size) = conn_lock.build_payload(&mut buf, reserved_pn.pn);
                    (reserved_pn, flags, size)
                };

                if let Err(_send_err) = l0_conn.send_pn(reserved_pn, flags, &buf[..size]).await {
                    todo!();
                }
            }

            send_notified.await;
        }
    };

    let conn_timeout = async {
        loop {
            let next_timeout = *last_recv_time.lock() + CONNECTION_TIMEOUT;
            if next_timeout < Instant::now() {
                return;
            }
            tokio::time::sleep_until(next_timeout).await;
        }
    };

    let keep_alive = async {
        loop {
            let next_keepalive = {
                let mut inner = conn.inner_mut.lock();
                if inner.last_send_time.elapsed() > KEEP_ALIVE_DELAY {
                    inner.set_force_send(true, &conn.notify_send);
                    Instant::now() + KEEP_ALIVE_DELAY
                } else {
                    inner.last_send_time + KEEP_ALIVE_DELAY
                }
            };

            tokio::time::sleep_until(next_keepalive).await;
        }
    };

    let pto_loop = async {
        let mut probe_timeout_mut = probe_timeout.clone();
        loop {
            // This is the deadline send_pto is currently waiting on with sleep_until.
            let current_deadline = Mutex::new(probe_timeout_mut.borrow_and_update().to_owned());

            // This future sleeps until the probe timeout deadline is elapsed but may overshoot
            // if the deadline ever decreases
            let send_pto = async {
                while let Some(deadline) = {
                    let dl = *current_deadline.lock();
                    dl
                } {
                    if deadline.elapsed().is_zero() {
                        tokio::time::sleep_until(deadline).await;
                    } else {
                        conn.trigger_pto();
                    }
                    *current_deadline.lock() = *probe_timeout.borrow();
                }
                pending::<()>().await;
            };

            // This future will end if the deadline changes to be sooner than the current_deadline
            // or if the watch sender has been dropped.
            let deadline_decreased = async {
                while probe_timeout_mut.changed().await.is_ok() {
                    let new_deadline = probe_timeout_mut.borrow().to_owned();
                    let current_deadline = *current_deadline.lock();
                    match (current_deadline, new_deadline) {
                        (None, Some(_)) => return Ok(()),
                        (Some(old_deadline), Some(new_deadline)) if new_deadline < old_deadline => {
                            return Ok(());
                        }
                        _ => {}
                    }
                }
                Err(())
            };

            // Race send_pto and deadline_decreased, this means send_pto is cancelled if the
            // deadline changes to be earlier than what send_pto is currently waiting for.
            select! {
                _ = send_pto => {}
                result = deadline_decreased => {
                    if result.is_err() {
                        break;
                    }
                }
            }
        }
    };

    select! {
        _ = recv_loop => {}
        _ = send_loop => {}
        _ = keep_alive => {}
        _ = pto_loop => {}
        _ = conn_timeout => {
            conn.inner_mut.lock().log("Stopping driver because connection has timed out")
        }
        _ = conn.closed() => {
            conn.inner_mut.lock().log("Stopping driver because the connection was closed")
        }
    }

    conn.close();

    let _ = l0_conn.send(2, b"").await;
}

#[cfg(test)]
mod tests {
    use std::net::SocketAddr;

    use layer0::{runtime::tokio::TokioRuntime, socket::SimulatedConditions};
    use runtime_trait::mock_socket::{MockNetwork, MockSocket};

    use crate::test_utils::drive_async_test;

    use super::*;

    type Endpoint<T> = layer0::endpoint::Endpoint<T, TokioRuntime>;
    type SimulatedNetworkConditionsSocket<T> =
        layer0::socket::SimulatedNetworkConditionsSocket<T, TokioRuntime>;

    async fn make_sock(
        network: &MockNetwork,
        conditions: SimulatedConditions,
    ) -> (SimulatedNetworkConditionsSocket<MockSocket>, SocketAddr) {
        let sock = network.make_sock("127.0.0.1:0".parse().unwrap()).unwrap();
        let addr = sock.local_addr();
        (
            SimulatedNetworkConditionsSocket::new(sock, conditions),
            addr,
        )
    }

    async fn make_conn_pair(
        conditions: SimulatedConditions,
    ) -> (
        NewConnection<SimulatedNetworkConditionsSocket<MockSocket>>,
        NewConnection<SimulatedNetworkConditionsSocket<MockSocket>>,
    ) {
        let network = MockNetwork::new();
        let (host_sock, host_addr) = make_sock(&network, conditions).await;
        let (client_sock, _client_addr) = make_sock(&network, Default::default()).await;

        let (_, mut host_incoming) = Endpoint::new(host_sock);
        let (client_ep, _) = Endpoint::new(client_sock);

        let conn1_fut = async { Connection::new(host_incoming.recv().await.unwrap().accept()) };
        let conn2_fut = async { Connection::new(client_ep.connect(host_addr).await.unwrap()) };

        tokio::join!(conn1_fut, conn2_fut)
    }

    #[test]
    fn connection_times_out() {
        drive_async_test(async {
            let ((conn1, _conn1_msgs), (_conn2, _conn2_msgs)) =
                make_conn_pair(Default::default()).await;

            tokio::time::sleep(CONNECTION_TIMEOUT * 2).await;
            assert!(!conn1.is_closed());

            conn1.inner_mut.lock().conn.socket().set_packet_loss(1.);

            tokio::time::sleep(CONNECTION_TIMEOUT * 2).await;
            assert!(conn1.is_closed());
        });
    }

    #[test]
    fn test_send_ro() {
        drive_async_test(async {
            tokio::time::resume();
            println!("Connecting");
            let ((conn1, _conn1_msgs), (_conn2, mut conn2_msgs)) =
                make_conn_pair(SimulatedConditions {
                    delay: Duration::from_millis(50),
                    packet_loss: 0.1,
                })
                .await;
            conn1.set_name("conn1");
            println!("Connection established");

            let count = 40;
            tokio::spawn(async move {
                let mut buf = [0; 450];
                for i in 0..count {
                    buf[0] = i as u8;
                    conn1.send(&buf, Reliability::Reliable, Ordering::Ordered);
                    //tokio::time::sleep(Duration::from_millis(5)).await;
                }
            });

            let recv_fut = async {
                let mut buf = [0; 450];
                for i in 0..count {
                    let msg = conn2_msgs.recv().await.unwrap();
                    buf[0] = i as u8;
                    assert_eq!(msg.as_ref(), &buf);
                    println!("received {i}");
                }
            };

            tokio::time::timeout(Duration::from_secs(300), recv_fut)
                .await
                .expect("recv took too long");
        });
    }
}
